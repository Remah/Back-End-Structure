const mongoose = require("mongoose");
const UserModel = require("./_user.model");

const schema = new mongoose.Schema(
  {
    rating: {
      type: Number,
      default: 0,
    },
    details:{
      type: String
    },
    description:{
      type: String
    },
    isAllowedUpdateVideos:{
      type: Boolean,
      default: true
    }
  },
  { discriminatorKey: "role" }
);
module.exports = UserModel.discriminator("teacher", schema);
