const mongoose = require("mongoose");
const UserModel = require("./_user.model");

const schema = new mongoose.Schema(
  {
    favourites: [
      {
        type: Number,
        ref: "course",
      },
    ],
    recommended:[{
      type: Number,
      ref: 'course'
    }],
    lastCategorySurvey: {
      type: Number,
      ref:'secondSubCategory'
    },
    lastCategoryFavourite: {
      type: Number,
      ref:'secondSubCategory'
    },
    lastCategorySearch: {
      type: Number,
      ref:'secondSubCategory'
    }
  },
  { discriminatorKey: "role" }
);
module.exports = UserModel.discriminator("student", schema);
