const mongoose = require("mongoose");
const $baseModel = require("./$baseModel");

const schema = new mongoose.Schema(
  {
    deviceToken:{
        type: String,
        required: true
    },
    courses:[{
        type:Number,
        ref:'course'
    }],
    lastCategorySurvey: {
      type: Number,
      ref:'secondSubCategory'
    },
    lastCategorySearch: {
      type: Number,
      ref:'secondSubCategory'
    }
  },
  { timestamps: true }
);

const response = (doc) => {
  return {
    id: doc.id,
    deviceToken: doc.deviceToken,
    courses: doc.courses,
    lastCategory: doc.lastCategory,
    lastCategorySurvey: doc.lastCategorySurvey,
    lastCategorySearch: doc.lastCategorySearch
    // createdAt: doc.createdAt,
    // updatedAt: doc.updatedAt,
  };
};

module.exports = $baseModel("recommend", schema, {
  responseFunc: response,
});
