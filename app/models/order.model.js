const mongoose = require("mongoose");
const $baseModel = require("./$baseModel");

const schema = new mongoose.Schema(
  {
    user: {
      type: Number,
      ref: "user",
    },
    subject: {
        type: Number,
        refPath: "subjectType",
        required: true,
    },
    subjectType: {
    type: String,
    enum: ["live", "course"],
    required: true,
    },
    totalPrice:{
        type: Number,
        required: true
    },
    isGift: {
      type: Boolean,
      default: false
    }
  },
  { timestamps: true }
);

const response = (doc) => {
  return {
    id: doc.id,
    user: doc.user,
    subject: doc.subject,
    subjectType: doc.subjectType,
    totalPrice: doc.totalPrice,
    isGift: doc.isGift,
    createdAt: doc.createdAt,
    updatedAt: doc.updatedAt,
  };
};

module.exports = $baseModel("order", schema, {
  responseFunc: response,
});
