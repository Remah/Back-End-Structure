const mongoose = require("mongoose");
const $baseModel = require("./$baseModel");

const schema = new mongoose.Schema(
  {
    nameAr: {
      type: String,
      required: true,
    },
    nameEn: {
      type: String,
      required: true,
    },
    photo: {
      type: String,
    },
    category:{
        type: Number,
        ref: 'category'
    },
    subCategory:{
        type: Number,
        ref: 'subCategory'
    }
  },
  { timestamps: true }
);

const response = (doc) => {
  const photo =
    doc.photo ||
    "https://res.cloudinary.com/nile-pharmacy/image/upload/v1558858260/assets/placeholder_a1ubee.jpg";

  return {
    id: doc.id,
    nameAr: doc.nameAr,
    nameEn: doc.nameEn,
    photo: photo,
    category: doc.category,
    subCategory: doc.subCategory,
    createdAt: doc.createdAt,
    updatedAt: doc.updatedAt,
  };
};

module.exports = $baseModel("secondSubCategory", schema, {
  responseFunc: response,
});
