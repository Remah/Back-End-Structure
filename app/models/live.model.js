const mongoose = require('mongoose');
const $baseModel = require('./$baseModel');

const schema = new mongoose.Schema(
  {
    category: {
      type: Number,
      ref: 'category'
    },
    subCategory: {
      type: Number,
      ref: "subCategory"
    },
    secondSubCategory: {
      type: Number,
      ref: "secondSubCategory"
    },
    description: {
      type: String,
    },
    nameAr: {
      type: String,
      required: true,
    },
    nameEn: {
      type: String,
      required: true,
    },
    photo: {
      type: String,
    },
    price: {
      type: Number
    },
    teacher: {
      type: Number,
      ref: 'user',
      required: true,
    },
    visibility: {
      type: Boolean,
      default: false,
    },
    passing_percentage: {
      type: Number,
      required: true,
    },
    liveStartedAt: {
      type: Date,
      required: true,
    },
    liveFinishedAt: {
      type: Date,
      required: true,
    },
    lecture: {
      type: Number,
      ref: 'lecture',
    },
    discount: {
      type: Number,
      ref: "discount",
    },
    hasOfferNow: {
      type: Boolean,
      default: false,
    },
    numberOfEnrolledStudents: {
      type: Number,
      default: 0,
    },
    discountStartedAt: {
      type: Date,
    },
    discountFinishedAt: {
      type: Date,
    },
  },
  { timestamps: true },
);

schema.virtual('isLive').get(function () {
  if (
    this.liveFinishedAt > new Date(new Date().toUTCString()) &&
    this.liveStartedAt <= new Date(new Date().toUTCString())
  )
    return true;
  return false;
});

const response = (doc) => {
  const photo =
    doc.photo ||
    'https://res.cloudinary.com/nile-pharmacy/image/upload/v1558858260/assets/placeholder_a1ubee.jpg';

  return {
    id: doc.id,
    category: doc.category,
    subCategory: doc.subCategory,
    secondSubCategory: doc.secondSubCategory,
    description: doc.description,
    numberOfEnrolledStudents: doc.numberOfEnrolledStudents,
    nameAr: doc.nameAr,
    nameEn: doc.nameEn,
    photo: photo,
    teacher: doc.teacher,
    price: doc.price,
    visibility: doc.visibility,
    passing_percentage: doc.passing_percentage,
    liveStartedAt: doc.liveStartedAt,
    liveFinishedAt: doc.liveFinishedAt,
    lecture: doc.lecture,
    isLive: doc.isLive,
    hasOfferNow: doc.hasOfferNow,
    discountStartedAt: doc.discountStartedAt,
    discountFinishedAt: doc.discountFinishedAt,
    discount: doc.discount,
    createdAt: doc.createdAt,
    updatedAt: doc.updatedAt,
  };
};

module.exports = $baseModel('live', schema, {
  responseFunc: response,
});
