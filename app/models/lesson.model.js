const mongoose = require("mongoose");
const $baseModel = require("./$baseModel");

const schema = new mongoose.Schema(
  {
    nameAr: {
      type: String,
      required: true,
    },
    nameEn: {
      type: String,
      required: true,
    },
    course: {
      type: Number,
      ref: "course",
      required: true,
    },
    section: {
      type: Number,
      ref: "section",
      required: true,
    },
    video: {
      type: String,
      required: true,
    },
    duration: {
      type: String,
      required: true,
    },
    pdf: {
      type: String,
      required: true,
    },
    views:{
      type: Number,
      default: 0
    }
  },
  { timestamps: true }
);

const response = (doc) => {
  return {
    id: doc.id,
    nameAr: doc.nameAr,
    nameEn: doc.nameEn,
    course: doc.course,
    section: doc.section,
    video: doc.video,
    duration: doc.duration,
    pdf: doc.pdf,
    views: doc.views,
    createdAt: doc.createdAt,
    updatedAt: doc.updatedAt,
  };
};

module.exports = $baseModel("lesson", schema, {
  responseFunc: response,
});
