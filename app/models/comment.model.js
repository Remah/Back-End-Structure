const mongoose = require("mongoose");
const $baseModel = require("./$baseModel");
const $baseSchema = require("./$baseSchema");

const schema = new mongoose.Schema(
  {
    user: {
      type: Number,
      ref: "user",
      required: true,
    },
    post: {
      type: Number,
      refPath: "type",
      required: true,
    },
    type: {
      type: String,
      enum: ["course", "lesson"],
    },
    content: {
      type: String,
    },
    image: {
      type: String,
    },
    replies: [replySchema()],
  },
  { timestamps: true }
);

const response = (doc) => {
  return {
    id: doc.id,
    user: doc.user,
    content: doc.content,
    image: doc.image,
    post: doc.post,
    type: doc.type,
    // replies: doc.replies,
    createdAt: doc.createdAt,
    updatedAt: doc.updatedAt,
  };
};

module.exports = $baseModel("comment", schema, {
  responseFunc: response,
});

// reply subdocument
function replySchema() {
  const schema = new mongoose.Schema(
    {
      user: {
        type: Number,
        ref: "user",
      },
      content: {
        type: String,
      },
      image: {
        type: String,
      },
    },
    { timestamps: true }
  );

  const response = (doc) => {
    return {
      id: doc.id,
      user: doc.user,
      content: doc.content,
      image: doc.image,
      createdAt: doc.createdAt,
      updatedAt: doc.updatedAt,
    };
  };

  return $baseSchema("comment.reply", schema, {
    responseFunc: response,
  });
}
