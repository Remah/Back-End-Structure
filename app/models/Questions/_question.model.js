const mongoose = require('mongoose');
const $baseModel = require('../$baseModel');
const schema = new mongoose.Schema(
  {
    head: {
      type: String,
      required: true,
    },
    image: {
      type: String,
    },
    addedBy: {
      type: Number,
      ref: 'user',
    },
    subject: {
      type: Number,
      refPath: 'subjectType',
      required: true,
    },
    subjectType: {
      type: String,
      enum: ['live', 'course'],
      required: true,
    },
  },
  { timestamps: true, discriminatorKey: 'type' },
);

const response = (doc) => {
  return {
    id: doc.id,
    section: doc.section,
    course: doc.course,
    type: doc.type,
    head: doc.head,
    image: doc.image,
    choices: doc.choices, // choose question
    subject: doc.subject,
    subjectType: doc.subjectType,
    modelAnswer: doc.modelAnswer,
    addedBy: doc.addedBy,
    createdAt: doc.createdAt,
    updatedAt: doc.updatedAt,
  };
};

module.exports = $baseModel('question', schema, {
  responseFunc: response,
});
