const mongoose = require("mongoose");
const $baseModel = require("./$baseModel");

const schema = new mongoose.Schema(
  {
    code: {
      type: String,
      required: true
    },
    discount: { 
      type: Number,
      required: true
    }
  },
  { timestamps: true }
);

const response = (doc) => {
  return {
    id: doc.id,
    code: doc.code,
    discount: doc.discount,
    createdAt: doc.createdAt,
    updatedAt: doc.updatedAt,
  };
};

module.exports = $baseModel("code", schema, {
  responseFunc: response,
});
