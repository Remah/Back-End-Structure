const mongoose = require("mongoose");
const $baseModel = require("./$baseModel");
const emailRules = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

const schema = new mongoose.Schema(
  {
    name:{
        type:String,
        required: true
    },
    details:{
        type:String
    },
    cv:{
        type: String,
        required:true
    },
    video: {
        type:String,
        required: true
    },
    photo:{
      type: String
    },
    // phone:{
    //   type: String,
    //   required: true
    // },
    // email: {
    //   type: String,
    //   match: emailRules,
    //   unique: true,
    //   index: true,
    //   sparse: true,
    // },
    // country:{
    //   type: String
    // },
    // gender:{
    //   type: String,
    //   enum : ['male','female']
    // }
  },
  { timestamps: true }
);

const response = (doc) => {
  return {
    // id: doc.id,
    name: doc.name,
    details:doc.details,
    cv: doc.cv,
    video: doc.video,
    photo: doc.photo,
    // phone: doc.phone,
    // email: doc.phone,
    // country: doc.country,
    // gender: doc.gender,
    createdAt: doc.createdAt,
    // updatedAt: doc.updatedAt,
  };
};

module.exports = $baseModel("request", schema, {
  responseFunc: response,
});
