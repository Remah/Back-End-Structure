const mongoose = require('mongoose');
const quizModel = require('./_quiz.model');
const schema = new mongoose.Schema(
  {
    course: {
      type: Number,
      ref: 'course',
    },
    section: {
      type: Number,
      ref: 'section',
    },
  },
  { timestamps: true, discriminatorKey: 'kind' },
);

module.exports = quizModel.discriminator('courseQuiz', schema);
