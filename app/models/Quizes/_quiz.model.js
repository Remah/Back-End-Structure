const mongoose = require('mongoose');
const $baseModel = require('../$baseModel');
const moment = require('moment');
const schema = new mongoose.Schema(
  {
    title: {
      type: String,
      required: true,
    },
    questions: [quizQuestionSchema()],
    availability: {
      type: Boolean,
      default: false,
    },
    type: {
      type: String,
      enum: ['section', 'mid', 'final'],
      required: true,
    },
  },
  { timestamps: true, discriminatorKey: 'kind' },
);

schema.virtual('points').get(function () {
  let points = 0;
  for (let i = 0; i < this.questions.length; i++) {
    points += this.questions[i].point;
  }
  return points;
});
const response = (doc) => {
  return {
    id: doc.id,
    title: doc.title,
    course: doc.course, // course quiz
    section: doc.section, // course quiz
    availability: doc.availability,
    points: doc.points,
    questions: doc.questions,
    type: doc.type,
    kind: doc.kind,
    duration: doc.duration, // Live quiz
    startedAt: doc.startedAt, // live quiz
    finshedAt:
      doc.kind === 'liveQuiz'
        ? moment(doc.startedAt).add(doc.duration, 'm')
        : undefined,
    live: doc.live, // live quiz
    isLive: doc.isLive, // live quiz
    createdAt: doc.createdAt,
    updatedAt: doc.updatedAt,
  };
};

module.exports = $baseModel('quiz', schema, {
  responseFunc: response,
});

function quizQuestionSchema() {
  const schema = new mongoose.Schema(
    {
      question: {
        type: Number,
        ref: 'question',
      },
      point: {
        type: Number,
      },
    },
    { _id: false },
  );

  schema.set('toJSON', {
    transform: function (doc) {
      return {
        question: doc.question,
        point: doc.point,
      };
    },
  });

  return schema;
}
