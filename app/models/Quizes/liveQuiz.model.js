const mongoose = require('mongoose');
const quizModel = require('./_quiz.model');
const moment = require('moment');
const schema = new mongoose.Schema(
  {
    live: {
      type: Number,
      ref: 'live',
    },
    startedAt: {
      type: Date,
      required: true,
    },
    duration: {
      type: Number,
      required: true,
    },
  },
  { timestamps: true, discriminatorKey: 'kind' },
);

schema.virtual('isLive').get(function () {
  let finishedAt = moment(this.startedAt).add(this.duration, 'm');
  if (
    this.startedAt <= new Date(new Date().toUTCString()) &&
    finishedAt > new Date(new Date().toUTCString())
  )
    return true;
  return false;
});

module.exports = quizModel.discriminator('liveQuiz', schema);
