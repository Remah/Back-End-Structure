const mongoose = require("mongoose");
const $baseModel = require("./$baseModel");

const schema = new mongoose.Schema(
  {
    points: {
        type: Number,
        required: true
    },
    student: {
        type: Number,
        ref: 'student'
    },
    category: {
        type: Number,
        ref:'category'
    }
  },
  { timestamps: true }
);

const response = (doc) => {
  return {
    id: doc.id,
    points: doc.points,
    user: doc.user,
    category: doc.category,
    createdAt: doc.createdAt,
    updatedAt: doc.updatedAt,
  };
};

module.exports = $baseModel("point", schema, {
  responseFunc: response,
});
