const mongoose = require("mongoose");
const $baseModel = require("./$baseModel");
const emailRules = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

const schema = new mongoose.Schema(
  {
    order:{
      type: Number,
      ref: 'order'
    },
    sender:{
      type: Number,
      ref: 'user'
    },
    used:{
      type: Boolean,
      default: false
    },
    receiver: {
      type: String,
      match: emailRules
    }
  },
  { timestamps: true }
);

const response = (doc) => {
  return {
    id: doc.id,
    order: doc.order,
    sender: doc.sender,
    used: doc.used,
    receiver: doc.receiver,
    createdAt: doc.createdAt,
    updatedAt: doc.updatedAt,
  };
};

module.exports = $baseModel("gift", schema, {
  responseFunc: response,
});
