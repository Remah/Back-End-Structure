const mongoose = require('mongoose');
const $baseModel = require('./$baseModel');

const schema = new mongoose.Schema(
  {
    live: {
      type: Number,
      ref: 'live',
      required: true,
    },
    title: {
      type: String,
    },
    type: {
      type: String,
      enum: ['pdf', 'video'],
      required: true,
    },
    link: {
      type: String,
      required: true,
    },
    duration: {
      type: Number,
    },
  },
  { timestamps: true },
);

const response = (doc) => {
  return {
    id: doc.id,
    title: doc.title,
    type: doc.type,
    link: doc.link,
    live: doc.live,
    duration: doc.duration,
    createdAt: doc.createdAt,
    updatedAt: doc.updatedAt,
  };
};

module.exports = $baseModel('material', schema, {
  responseFunc: response,
});
