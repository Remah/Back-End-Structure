const mongoose = require('mongoose');
const $baseModel = require('./$baseModel');

const schema = new mongoose.Schema(
  {
    title: {
      type: String,
      required: true,
    },
    live: {
      type: Number,
      ref: 'live',
      required: true,
    },
    liveUrl: {
      type: String,
    },
    time: {
      type: Date,
      required: true,
    },
    videoUrl: {
      type: String,
    },
    users:[
      {
      type: Number,
      ref: 'user'
      }
  ]
  },
  { timestamps: true },
);

const response = (doc) => {
  return {
    id: doc.id,
    title: doc.title,
    users: doc.users,
    live: doc.live,
    liveUrl: doc.liveUrl,
    time: doc.time,
    videoUrl: doc.videoUrl,
    createdAt: doc.createdAt,
    updatedAt: doc.updatedAt,
  };
};

module.exports = $baseModel('lecture', schema, {
  responseFunc: response,
});
