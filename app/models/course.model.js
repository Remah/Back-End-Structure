const mongoose = require("mongoose");
const $baseModel = require("./$baseModel");

const schema = new mongoose.Schema(
  {
    category: {
      type: Number,
      ref: "category"
    },
    subCategory: {
      type: Number,
      ref: "subCategory"
    },
    secondSubCategory: {
      type: Number,
      ref: "secondSubCategory"
    },
    description: {
      type: String,
    },
    include: {
      type: [String],
    },
    nameAr: {
      type: String,
      required: true,
    },
    nameEn: {
      type: String,
      required: true,
    },
    photo: {
      type: String,
    },
    price: {
      type: Number,
      required: true,
    },
    teacher: {
      type: Number,
      ref: "user",
      required: true,
    },
    objectives: {
      type: [String],
    },
    requirements: {
      type: [String],
    },
    rating: {
      type: Number,
      default: 0,
    },
    discount: {
      type: Number,
      ref: "discount",
    },
    visibility: {
      type: Boolean,
      default: false,
    },
    hasOfferNow: {
      type: Boolean,
      default: false,
    },
    level: {
      type: String,
      enum: ["All", "Beginner", "Intermediate", "Expert"],
    },
    language: {
      type: String,
    },
    numberOfHours: {
      type: Number,
      required: true,
    },
    numberOfEnrolledStudents: {
      type: Number,
      default: 0,
    },
    video: {
      type: String,
    },
    passing_percentage:{
      type:Number,
      required: true
    },
    discountStartedAt: {
      type: Date,
    },
    discountFinishedAt: {
      type: Date,
    },
    status:{
      type: String,
      enum: ['accepted','pending'],
      default: 'pending'
    }
  },
  { timestamps: true }
);

const response = (doc) => {
  const photo =
    doc.photo ||
    "https://res.cloudinary.com/nile-pharmacy/image/upload/v1558858260/assets/placeholder_a1ubee.jpg";

  return {
    id: doc.id,
    status: doc.status,
    level: doc.level,
    language: doc.language,
    numberOfHours: doc.numberOfHours,
    numberOfEnrolledStudents: doc.numberOfEnrolledStudents,
    video: doc.video,
    category: doc.category,
    subCategory: doc.subCategory,
    secondSubCategory: doc.secondSubCategory,
    description: doc.description,
    objectives: doc.objectives,
    requirements: doc.requirements,
    include: doc.include,
    nameAr: doc.nameAr,
    nameEn: doc.nameEn,
    photo: photo,
    teacher: doc.teacher,
    price: doc.price,
    rating: doc.rating,
    hasOfferNow: doc.hasOfferNow,
    discountStartedAt: doc.discountStartedAt,
    discountFinishedAt: doc.discountFinishedAt,
    discount: doc.discount,
    visibility: doc.visibility,
    passing_percentage: doc.passing_percentage,
    createdAt: doc.createdAt,
    updatedAt: doc.updatedAt,
  };
};

module.exports = $baseModel("course", schema, {
  responseFunc: response,
});
