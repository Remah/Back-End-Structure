const mongoose = require('mongoose');
const $baseModel = require('./$baseModel');

const schema = new mongoose.Schema(
  {
    user: {
      type: Number,
      ref: 'user'
    },
    head: {
      type: String,
      required: true
    },
    answer: {
      type: String
    }
  },
  { timestamps: true }
);

const response = doc => {
  return {
    id: doc.id,
    user: doc.user,
    head: doc.head,
    answer: doc.answer,
    createdAt: doc.createdAt,
    updatedAt: doc.updatedAt
  };
};

module.exports = $baseModel('inquiry', schema, {
  responseFunc: response
});
