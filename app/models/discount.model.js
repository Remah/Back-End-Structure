const mongoose = require("mongoose");
const $baseModel = require("./$baseModel");

const schema = new mongoose.Schema(
  {
    subject: {
      type: Number,
      refPath: "subjectType"
    },
    subjectType: {
      type: String,
      enum: ["live", "course"]
    },
    percentage: {
      type: Number,
      required: true,
    },
    startedAt: {
      type: Date,
    },
    finishedAt: {
      type: Date,
    },
  },

  { timestamps: true }
);
schema.virtual("isValid").get(function () {
  if (
    this.finishedAt > new Date(new Date().toUTCString()) &&
    this.startedAt <= new Date(new Date().toUTCString())
  )
    return true;
  return false;
});

schema.virtual("isExpired").get(function () {
  if (this.finishedAt <= new Date(new Date().toUTCString())) return true;
  return false;
});

const response = (doc) => {
  return {
    // id: doc.id,
    percentage: doc.percentage,
    subject: doc.subject,
    subjectType: doc.subjectType,
    startedAt: doc.startedAt,
    finishedAt: doc.finishedAt,
    isValid: doc.isValid,
    isExpired: doc.isExpired,
    // createdAt: doc.createdAt,
    // updatedAt: doc.updatedAt
  };
};

module.exports = $baseModel("discount", schema, {
  responseFunc: response,
});
