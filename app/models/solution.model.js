const mongoose = require('mongoose');
const { solution } = require('.');
const $baseModel = require('./$baseModel');

const schema = new mongoose.Schema(
  {
    quiz: {
      type: Number,
      ref: 'quiz',
    },
    student: {
      type: Number,
      ref: 'student',
    },
    status: {
      type: String,
      enum: ['solving', 'done'],
    },
    submittedAt: {
      type: Date,
      default: null,
    },
    questions: [
      new mongoose.Schema(
        {
          question: {
            type: Number,
            required: true,
            ref: 'question',
          },
          correct: {
            type: Boolean,
            default: null,
          },
          mark: {
            type: Number,
            default: 0,
          },
        },
        { discriminatorKey: 'type', _id: false },
      ),
    ],
  },
  { timestamps: true },
);

schema.virtual('mark').get(function () {
  let totalMark = 0;
  this.questions.forEach((question) => (totalMark += question.mark));
  return totalMark;
});

schema.methods.turnToChecking = async function (quiz) {
  let examObj = {};
  quiz.questions.forEach((q) => {
    examObj[q.question.id] = q;
  });
  this.questions.forEach((obj) => {
    // if (!obj.question) return; // if teacher delete question later , then when populate question will be null
    // if (examObj[obj.question.id] === undefined) return;
    if (examObj[obj.question.id].question.modelAnswer === obj.answer) {
      obj.correct = true;
      obj.mark = examObj[obj.question.id].point;
    } else obj.correct = false;
  });
  this.status = 'done';
  this.submittedAt = new Date(new Date().toUTCString());
  await this.save();
};

schema.path('questions').discriminator(
  'choose',
  new mongoose.Schema(
    {
      answer: {
        type: Number,
      },
    },
    { _id: false },
  ),
);

schema.path('questions').discriminator(
  'truefalse',
  new mongoose.Schema(
    {
      answer: {
        type: Boolean,
      },
    },
    { _id: false },
  ),
);

const response = (doc) => {
  return {
    id: doc.id,
    quiz: doc.quiz,
    student: doc.student,
    status: doc.status,
    questions: doc.questions,
    mark: doc.mark,
    submittedAt: doc.submittedAt,
    createdAt: doc.createdAt,
    updatedAt: doc.updatedAt,
  };
};

module.exports = $baseModel('solution', schema, {
  responseFunc: response,
});
