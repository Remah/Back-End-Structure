const mongoose = require('mongoose');
const $baseModel = require('../$baseModel');

const schema = new mongoose.Schema(
  {
    user: {
      type: Number,
      ref: 'user',
      required: true,
    },
    isCertificated: {
      type: Boolean,
      default: false,
    },
    teacher:{
      type: Number,
      ref: 'teacher'
    }
  },
  { timestamps: true, discriminatorKey: 'kind' },
);

const response = (doc) => {
  return {
    id: doc.id,
    user: doc.user,
    teacher: doc.teacher,
    course: doc.course,
    live: doc.live,
    numberOfCourseLessons: doc.numberOfCourseLessons,
    lessons: doc.lessons,
    isCertificated: doc.isCertificated,
    isCompleted: doc.isCompleted,
    kind: doc.kind
    // createdAt: doc.createdAt,
    // updatedAt: doc.updatedAt
  };
};

module.exports = $baseModel('progress', schema, {
  responseFunc: response,
});
