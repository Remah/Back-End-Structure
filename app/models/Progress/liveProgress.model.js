const mongoose = require('mongoose');
const progressModel = require('./_progress.model');
const schema = new mongoose.Schema(
  {
    live: {
      type: Number,
      ref: 'live',
    },
  },
  { timestamps: true, discriminatorKey: 'kind' },
);

module.exports = progressModel.discriminator('liveProgress', schema);
