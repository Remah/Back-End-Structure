const mongoose = require('mongoose');
const progressModel = require('./_progress.model');
const schema = new mongoose.Schema(
  {
    course: {
      type: Number,
      ref: 'course',
    },
    lessons: [
      {
        type: Number,
        ref: 'lesson',
      },
    ],
    numberOfCourseLessons: {
      type: Number,
      required: true,
    },
  },
  { timestamps: true, discriminatorKey: 'kind' },
);

schema.virtual('isCompleted').get(function () {
  if (this.numberOfCourseLessons === this.lessons.length) return true;

  return false;
});

module.exports = progressModel.discriminator('courseProgress', schema);
