const express = require('express');
const policies = require('../policies');
const ctrls = require('../controllers');
const resources = require('./resources');
const passport = require('passport');
require('../services/passport');

let apiRouter = express.Router();

// public
apiRouter.post('/signup', ctrls.AuthCtrl.signup);
apiRouter.post('/login', ctrls.AuthCtrl.login);
apiRouter.post('/verify', ctrls.AuthCtrl.verify);
apiRouter.post('/resend', ctrls.AuthCtrl.resendCode);
apiRouter.post('/reset-password', ctrls.AuthCtrl.reset);
apiRouter.post(
  '/login-google',
  passport.authenticate('googleToken1', { session: false }),
  ctrls.AuthCtrl.loginGoogle,
);
apiRouter.post('/login-facebook', ctrls.AuthCtrl.loginFacebook);
apiRouter.get('/category/:id', ctrls.CategoryCtrl.fetchOne);
apiRouter.get('/category', ctrls.CategoryCtrl.fetchAll);
apiRouter.get('/category/:id/sub-category', ctrls.SubCategoryCtrl.fetchAll);
apiRouter.get('/sub-category/:id', ctrls.SubCategoryCtrl.fetchOne);
apiRouter.get('/sub-category/:id/second-sub-category', ctrls.SecondSubCategoryCtrl.fetchAll);
apiRouter.get('/second-sub-category/:id', ctrls.SecondSubCategoryCtrl.fetchOne);
apiRouter.get('/all/second-sub-category',ctrls.SecondSubCategoryCtrl.fetchAllInSystem)
apiRouter.get('/course/:id', ctrls.CourseCtrl.fetchOne);
apiRouter.get('/courses/search',ctrls.CourseCtrl.fetchAllSearch)
apiRouter.get('/second-sub-category/:id/course', ctrls.CourseCtrl.fetchAllSSC);
apiRouter.get('/courses', ctrls.CourseCtrl.fetchAll);
apiRouter.get('/courses/:id/related', ctrls.CourseCtrl.relatedCourses);
apiRouter.get('/courses/:id/rate', ctrls.RateCtrl.fetchDetails);
apiRouter.get('/courses/:id/all-rate', ctrls.RateCtrl.fetchAllPagination);
apiRouter.get('/teachers/:id/rate', ctrls.RateCtrl.fetchDetails);
apiRouter.get('/teachers/:id/all-rate', ctrls.RateCtrl.fetchAllPagination);
apiRouter.get('/top/teachers', ctrls.StatisticalCtrl.topRatedTeachers);
apiRouter.get('/top/courses', ctrls.StatisticalCtrl.topRatedCourses);
apiRouter.get('/offers', ctrls.CourseCtrl.fetchOffers);
apiRouter.post('/submit', ctrls.TeacherCtrl.submitRequest);
apiRouter.post('/recommend', ctrls.RecommendationCtrl.addCourse);
apiRouter.post('/for-you', ctrls.RecommendationCtrl.forYou);
apiRouter.get('/second-sub-category/:id/live', ctrls.LiveCtrl.fetchAll);
apiRouter.get('/live/recommendation', ctrls.LiveCtrl.fetchAllCurrent);
apiRouter.get('/live/:id', ctrls.LiveCtrl.fetchOne);
apiRouter.get('/teacher/:id/info',ctrls.CourseCtrl.fetchTeacherWithCourses)
apiRouter.get("/course/:id/section", ctrls.SectionCtrl.fetchAll);
apiRouter.get("/section/:id", ctrls.SectionCtrl.fetchOne);
apiRouter.get('/course/:id/check',ctrls.CourseCtrl.checkIfBought)
apiRouter.get('/live/:id/check',ctrls.LiveCtrl.checkIfBought)
apiRouter.post('/test', ctrls.AuthCtrl.test);

// private
apiRouter.use(policies.isAuthenticated);

// populate all resources
for (let key of Object.keys(resources)) {
  let resource = resources[key];
  apiRouter.use(resource);
}

module.exports = apiRouter;
