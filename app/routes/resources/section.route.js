const express = require("express");
const ctrls = require("../../controllers");
const policies = require("../../policies");
let router = express.Router();

router.post(
  "/course/:id/section",
  policies.isAllowed(["admin", "teacher"]),
  ctrls.SectionCtrl.createOne
);
router.patch(
  "/section/:id",
  policies.isAllowed(["admin", "teacher"]),
  ctrls.SectionCtrl.updateOne
);
router.delete(
  "/section/:id",
  policies.isAllowed(["admin", "teacher"]),
  ctrls.SectionCtrl.deleteOne
);



module.exports = router;
