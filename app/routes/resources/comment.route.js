const express = require("express");
const ctrls = require("../../controllers");

let router = express.Router();

router.post("/lessons/:id/comments", ctrls.commentCtrl.createOne);
// router.post("/courses/:id/comments", ctrls.commentCtrl.createOne);
router.get("/comments/:id", ctrls.commentCtrl.fetchOne);
router.get("/lessons/:id/comments", ctrls.commentCtrl.fetchAll);
router.put("/comments/:id", ctrls.commentCtrl.updateOne);
router.delete("/comments/:id", ctrls.commentCtrl.deleteOne);

module.exports = router;
