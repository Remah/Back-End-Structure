const express = require("express");
const ctrls = require("../../controllers");

let router = express.Router();

router.get('/gifts/all',ctrls.GiftCtrl.fetchAllGifts)
router.get('/gifts/unused',ctrls.GiftCtrl.fetchUnusedGifts)
router.get('/gifts/:id',ctrls.GiftCtrl.fetchOne)
router.post('/gifts/:id/accept',ctrls.GiftCtrl.accept)

module.exports = router;
