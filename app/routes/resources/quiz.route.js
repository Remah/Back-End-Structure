const express = require('express');
const ctrls = require('../../controllers');
const policies = require('../../policies');

let router = express.Router();

router.post('/section/:id/quiz', ctrls.QuizCtrl.createOneSection);
router.post('/course/:id/quiz', ctrls.QuizCtrl.createOneFinalCourse);
router.post('/live/:id/quiz', ctrls.QuizCtrl.createOneLive);
router.patch('/quiz/:id', ctrls.QuizCtrl.updateOne);
router.post('/quiz/:id/questions-new', ctrls.QuizCtrl.addManyQuestionsNew);
router.post('/quiz/:id/questions', ctrls.QuizCtrl.addManyQuestions);
router.delete('/quiz/:eid/questions/:qid', ctrls.QuizCtrl.deleteQuestion);
router.patch(
  '/quiz/:eid/questions/:qid',
  ctrls.QuizCtrl.updatePointOfQuestion,
);
router.get('/quiz/:id', ctrls.QuizCtrl.fetchOneQuiz); //
router.get('/section/:id/quiz', ctrls.QuizCtrl.fetchSectionQuiz); 
router.get('/course/:id/quiz', ctrls.QuizCtrl.fetchCourseQuiz);
router.get('/live/:id/quiz', ctrls.QuizCtrl.fetchLiveCourseQuizes);

router.post(
  '/quiz/:id/start',
  policies.isAllowed('student'),
  ctrls.QuizCtrl.startExam,
);
router.patch(
  '/quiz/:id/submit',
  policies.isAllowed('student'),
  ctrls.QuizCtrl.submitSolution,
);
router.patch(
  '/quiz/:id/done',
  policies.isAllowed('student'),
  ctrls.QuizCtrl.doneSolvingExam,
);

router.delete('/quiz/:id',ctrls.QuizCtrl.deleteQuiz)

module.exports = router;
