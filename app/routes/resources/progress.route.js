const express = require('express');
// const policies = require("../../policies");
const ctrls = require('../../controllers');

const router = express.Router();

// router.post('/course/:id/enroll', ctrls.ProgressCtrl.enroll);

router.get('/myStatistics', ctrls.ProgressCtrl.myStatistics);
router.get('/myCourses', ctrls.ProgressCtrl.myCourses);
router.get('/myLives', ctrls.ProgressCtrl.myLives);
router.post('/lesson/:id/progress', ctrls.ProgressCtrl.addLessonToMyProgress);

module.exports = router;
