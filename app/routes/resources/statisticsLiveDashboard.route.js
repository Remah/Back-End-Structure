const express = require("express");
const ctrls = require("../../controllers");
let router = express.Router();

router.get('/live/:id/statistics',ctrls.statisticsLiveDashboard.fetchAllStudents)
router.get('/live/:lid/students/:sid/exams',ctrls.statisticsLiveDashboard.fetchSolutionsStudent)
router.get('/live/:lid/student/:sid/lectures',ctrls.statisticsLiveDashboard.fetchAttendacesToStudent)

module.exports = router;
