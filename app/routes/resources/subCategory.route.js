const express = require("express");
const ctrls = require("../../controllers");
const policies = require("../../policies");
let router = express.Router();

router.post(
  "/category/:id/sub-category",
  policies.isAllowed("admin"),
  ctrls.SubCategoryCtrl.createOne
);
router.patch(
  "/sub-category/:id",
  policies.isAllowed("admin"),
  ctrls.SubCategoryCtrl.updateOne
);
router.delete(
  "/sub-category/:id",
  policies.isAllowed("admin"),
  ctrls.SubCategoryCtrl.deleteOne
);

module.exports = router;
