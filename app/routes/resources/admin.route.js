const express = require("express");
const ctrls = require("../../controllers");
const policies = require("../../policies");
let router = express.Router();

router.get('/requests',policies.isAllowed('admin'),ctrls.AdminCtrl.fetchSubmits)

router.post('/register/teacher',policies.isAllowed('admin'),ctrls.AdminCtrl.addTeacher)

router.get('/students',policies.isAllowed('admin'),ctrls.AdminCtrl.fetchAllStudents)

router.get('/teachers',policies.isAllowed('admin'),ctrls.AdminCtrl.fetchAllTeachers)

router.get('/statistics/general',policies.isAllowed('admin'),ctrls.AdminCtrl.generalStatistics)

router.patch('/teachers/:id/allow-update-videos',policies.isAllowed('admin'),ctrls.AdminCtrl.allowTeacherToUpdateVideos)

module.exports = router;
