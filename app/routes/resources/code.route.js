const express = require("express");
const ctrls = require("../../controllers");
const policies = require("../../policies");

let router = express.Router();

router.get('/codes/:id',policies.isAllowed('admin'),ctrls.CodeCtrl.fetchOne)
router.put('/codes/:id',policies.isAllowed('admin'),ctrls.CodeCtrl.updateOne)
router.delete('/codes/:id',policies.isAllowed('admin'),ctrls.CodeCtrl.deleteOne)
router.get('/codes',policies.isAllowed('admin'),ctrls.CodeCtrl.fetchAll)
router.post('/codes',policies.isAllowed('admin'),ctrls.CodeCtrl.createOne)
router.post('/check-code',ctrls.CodeCtrl.checkIfValid)

module.exports = router;
