const express = require('express');
const ctrls = require('../../controllers');
const policies = require('../../policies');

const router = express.Router();

router.post('/second-sub-category/:id/live', policies.isAllowed('teacher'), ctrls.LiveCtrl.createOne);
router.patch('/live/:id', ctrls.LiveCtrl.updateOne);
router.delete('/live/:id', ctrls.LiveCtrl.deleteOne);

// teacher 
router.get('/teacher/live',policies.isAllowed('teacher'),ctrls.LiveCtrl.fetchTeacherLives)

// Calender
router.get('/live/:id/calender',ctrls.LiveCtrl.calender)

module.exports = router;
