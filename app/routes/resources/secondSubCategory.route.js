const express = require("express");
const ctrls = require("../../controllers");
const policies = require("../../policies");
let router = express.Router();

router.post(
  "/sub-category/:id/second-sub-category",
  policies.isAllowed("admin"),
  ctrls.SecondSubCategoryCtrl.createOne
);
router.patch(
  "/second-sub-category/:id",
  policies.isAllowed("admin"),
  ctrls.SecondSubCategoryCtrl.updateOne
);
router.delete(
  "/second-sub-category/:id",
  policies.isAllowed("admin"),
  ctrls.SecondSubCategoryCtrl.deleteOne
);

module.exports = router;
