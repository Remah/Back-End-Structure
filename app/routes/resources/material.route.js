const express = require('express');
const policies = require('../../policies');
const ctrls = require('../../controllers');

let router = express.Router();

router.post('/live/:id/videos', ctrls.materialCtrl.CreateOne);
router.post('/live/:id/pdf', ctrls.materialCtrl.CreateOne);

router.patch('/videos/:id', ctrls.materialCtrl.updateOne);
router.patch('/pdf/:id', ctrls.materialCtrl.updateOne);

router.delete('/videos/:id', ctrls.materialCtrl.DeleteOne);
router.delete('/pdf/:id', ctrls.materialCtrl.DeleteOne);

router.get('/live/:id/videos', ctrls.materialCtrl.fetchAll);
router.get('/live/:id/pdf', ctrls.materialCtrl.fetchAll);
router.get('/videos/:id', ctrls.materialCtrl.fetchOne);
router.get('/pdf/:id', ctrls.materialCtrl.fetchOne);

module.exports = router;
