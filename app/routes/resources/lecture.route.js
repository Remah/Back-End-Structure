const express = require('express');
const ctrls = require('../../controllers');
let router = express.Router();

router.post('/live/:id/lecture', ctrls.LectureCtrl.createOne);
router.patch('/lecture/:id', ctrls.LectureCtrl.updateOne);
router.delete('/lecture/:id', ctrls.LectureCtrl.deleteOne);
router.get('/live/:id/lecture', ctrls.LectureCtrl.fetchCurrentLecture);
router.get('/live/:id/archive', ctrls.LectureCtrl.fetchArchiveLectures);
router.get('/lecture/:id/attendance',ctrls.LectureCtrl.enrollInLecture)
router.get('/lecture/:id/attendees',ctrls.LectureCtrl.fetchAttendanceByTeacher)
module.exports = router;
