const express = require("express");
const ctrls = require("../../controllers");
let router = express.Router();

router.get('/course/:id/statistics',ctrls.statisticsCourseDashboard.statisticalBasedOnCourse)
router.get('/course/:cid/student/:sid/progress',ctrls.statisticsCourseDashboard.statisticalBasedOnStudent)
router.get('/course/:cid/student/:sid/exams',ctrls.statisticsCourseDashboard.statisticsExamsBasedOnStudent)

module.exports = router;
