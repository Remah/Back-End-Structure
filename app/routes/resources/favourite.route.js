const express = require("express");
const policies = require("../../policies");
const ctrls = require("../../controllers");

const router = express.Router();

router.post(
  "/favourites/:id",
  policies.isAllowed("student"),
  ctrls.favouriteCtrl.AddOne
);
router.delete(
  "/favourites/:id",
  policies.isAllowed("student"),
  ctrls.favouriteCtrl.RemoveOne
);
router.get(
  "/favourites",
  policies.isAllowed("student"),
  ctrls.favouriteCtrl.fetchAll
);

module.exports = router;
