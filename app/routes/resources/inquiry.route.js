const express = require("express");
const policies = require("../../policies");
const ctrls = require("../../controllers");

const router = express.Router();

router.post(
  "/inquiry",
  policies.isAllowed("student"),
  ctrls.InquiryCtrl.createOne
);
router.patch(
  "/inquiry/:id",
  ctrls.InquiryCtrl.updateOne
);
router.delete(
  "/inquiry/:id",
  ctrls.InquiryCtrl.deleteOne
);

router.get(
    '/inquiry/:id',
    ctrls.InquiryCtrl.fetchOne
)

router.get(
    '/inquiry',
    ctrls.InquiryCtrl.fetchAll
)

module.exports = router;
