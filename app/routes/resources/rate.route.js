const express = require("express");
const ctrls = require("../../controllers");

let router = express.Router();

router.post("/teachers/:id/rate", ctrls.RateCtrl.createOne);
router.post("/courses/:id/rate", ctrls.RateCtrl.createOne);

module.exports = router;
