const express = require("express");
const ctrls = require("../../controllers");
const policies = require("../../policies");
let router = express.Router();

router.post(
  "/second-sub-category/:id/course",
  policies.isAllowed(["admin", "teacher"]),
  ctrls.CourseCtrl.createOne
);
router.patch(
  "/course/:id",
  policies.isAllowed(["admin", "teacher"]),
  ctrls.CourseCtrl.updateOne
);
router.delete(
  "/course/:id",
  policies.isAllowed(["admin", "teacher"]),
  ctrls.CourseCtrl.deleteOne
);

router.get(
  "/teacher/courses",
  policies.isAllowed('teacher'),
  ctrls.CourseCtrl.fetchTeacherCourses
)


module.exports = router;
