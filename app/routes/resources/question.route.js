const express = require('express');
const ctrls = require('../../controllers');
const policies = require('../../policies');

let router = express.Router();

router.post('/course/:id/questions', ctrls.QuestionCtrl.createMany);

router.post('/live/:id/questions', ctrls.QuestionCtrl.createMany);

router.post('/course/:id/question', ctrls.QuestionCtrl.createOne);
router.post('/live/:id/question', ctrls.QuestionCtrl.createOne);

router.delete(
  '/questions/:id',
  //   policies.isAllowed(["teacher", "admin"]),
  ctrls.QuestionCtrl.deleteOne,
);

router.patch(
  '/questions/:id',
  //   policies.isAllowed(["teacher", "admin"]),
  ctrls.QuestionCtrl.updateOne,
);

router.get(
  '/course/:id/questions',
  policies.isAllowed(['teacher', 'admin']),
  ctrls.QuestionCtrl.fetchQuestions,
);

router.get(
  '/live/:id/questions',
  policies.isAllowed(['teacher', 'admin']),
  ctrls.QuestionCtrl.fetchQuestions,
);

router.get(
  '/questions/:id',
  policies.isAllowed(['teacher', 'admin']),
  ctrls.QuestionCtrl.fetchOne,
);

module.exports = router;
