const express = require("express");
const ctrls = require("../../controllers");
const policies = require("../../policies");
let router = express.Router();

router.post( 
  "/section/:id/lesson",
  policies.isAllowed(["admin", "teacher"]),
  ctrls.LessonCtrl.createOne
);
router.patch(
  "/lesson/:id",
  policies.isAllowed(["admin", "teacher"]),
  ctrls.LessonCtrl.updateOne
);
router.delete(
  "/lesson/:id",
  policies.isAllowed(["admin", "teacher"]),
  ctrls.LessonCtrl.deleteOne
);

router.get('/lesson/:id/views',ctrls.LessonCtrl.viewsOfLesson)
router.get("/section/:id/lesson", ctrls.LessonCtrl.fetchAll);
router.get("/lesson/:id", ctrls.LessonCtrl.fetchOne);

module.exports = router;
