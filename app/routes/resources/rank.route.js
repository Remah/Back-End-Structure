const express = require('express');
const ctrls = require('../../controllers');

const router = express.Router();

router.get('/rank', ctrls.RankCtrl.rank);

module.exports = router;
