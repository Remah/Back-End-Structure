const $baseCtrl = require("../$baseCtrl");
const models = require("../../models");
const { APIResponse } = require("../../utils");

module.exports = $baseCtrl(async (req, res) => {
  let query = {}
  if(req.me.role === 'student')
    query.answer = {$exists: true}
  let inquires = await models.inquiry.fetchAll(
      req.allowPagination,
      {...req.queryFilter,...query},
      {...req.queryOptions,populate:{path:'user',select:'_id username  photo'}}
  )

  return APIResponse.Ok(res,inquires)
});


