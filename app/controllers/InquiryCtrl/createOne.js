const $baseCtrl = require("../$baseCtrl");
const models = require("../../models");
const { APIResponse } = require("../../utils");

module.exports = $baseCtrl(async (req, res) => {
  delete req.body.answer
  req.body.user = req.me.id
  let inquiry = await new models.inquiry(req.body).save()
  return APIResponse.Ok(res,inquiry)
});
