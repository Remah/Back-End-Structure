const $baseCtrl = require("../$baseCtrl");
const models = require("../../models");
const { APIResponse } = require("../../utils");

module.exports = $baseCtrl(async (req, res) => {
  const inquiry = await models.inquiry.findById(parseInt(req.params.id))
  if(!inquiry) return APIResponse.NotFound(res,'No inquiry with that id')
  if(req.me.role === 'student' && inquiry.user !== req.me.id)
    return APIResponse.Forbidden(res,'Dont allow to delete this question')

  await inquiry.delete()
  return APIResponse.NoContent(res)  
});


