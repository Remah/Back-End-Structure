const $baseCtrl = require("../$baseCtrl");
const models = require("../../models");
const { APIResponse } = require("../../utils");

module.exports = $baseCtrl(async (req, res) => {
  const inquiry = await models.inquiry.findById(parseInt(req.params.id)).populate('user')
  if(!inquiry) return APIResponse.NotFound(res,'No inquiry with that id')
  
  return APIResponse.Ok(res,inquiry)
});


