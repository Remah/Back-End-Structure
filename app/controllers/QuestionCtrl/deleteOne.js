const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');
const _ = require('lodash');

module.exports = $baseCtrl(async (req, res) => {
  const question = await models.question.findById(req.params.id);
  if (!question) return APIResponse.NotFound(res);

  if (question.addedBy !== req.me.id)
    return APIResponse.Forbidden(res, 'You dont allow to Delete this question');

  APIResponse.NoContent(res);

  let quizes = await models._quiz.find({
    'questions.question': question.id,
  });
  for (let i = 0; i < quizes.length; i++) {
    let key = _.findKey(quizes[i].questions, {
      question: question.id,
    });
    if (key === undefined) continue;
    quizes[i].questions.splice(key, 1);
    await quizes[i].save();
  }

  let solutions = await models.solution.find({
    'questions.question': question.id
  })
  for (let i = 0; i < solutions.length; i++) {
    let key = _.findKey(solutions[i].questions, {
      question: question.id,
    });
    if (key === undefined) continue;
    solutions[i].questions.splice(key, 1);
    await solutions[i].save();
  }

  await question.delete();

});
