const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');

module.exports = $baseCtrl(async (req, res) => {
  const question = await models.question
    .findById(parseInt(req.params.id))
    .populate('subject');

  if (!question)
    return APIResponse.NotFound(res, 'No question with that id');

  return APIResponse.Ok(res, question);
});
