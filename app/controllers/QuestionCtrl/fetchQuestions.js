const $baseCtrl = require('../$baseCtrl');
const { escapeRegExp } = require('lodash');
const models = require('../../models');
const { APIResponse } = require('../../utils');

module.exports = $baseCtrl(async (req, res) => {
  let routePath = req.route.path.split('/');
  let subjectType = routePath[1] === 'live' ? 'live' : 'course';

  const object = await models[subjectType].findById(
    parseInt(req.params.id),
  );
  if (!object)
    return APIResponse.NotFound(res, `No ${subjectType} with that id`);

  let query = {};
  query.subjectType = subjectType;
  query.subject = object.id;
  if (req.query.head) {
    query.head = new RegExp(escapeRegExp(req.query.head), 'i');
    delete req.queryFilter.head;
  }
  let Questions = await models.question.fetchAll(
    req.allowPagination,
    {
      ...req.queryFilter,
      ...query,
    },
    {
      ...req.queryOptions,
      populate: 'subject',
    },
  );

  return APIResponse.Ok(res, Questions);
});
