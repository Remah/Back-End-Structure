const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');

module.exports = $baseCtrl(async (req, res) => {
  let routePath = req.route.path.split('/');
  let subjectType = routePath[1] === 'live' ? 'live' : 'course';

  const object = await models[subjectType].findById(
    parseInt(req.params.id),
  );
  if (!object)
    return APIResponse.NotFound(res, `No ${subjectType} with that id`);

  if (object.teacher !== req.me.id)
    return APIResponse.Forbidden(
      res,
      'Dont allow to add questions in this section',
    );

  let user = req.authenticatedUser;

  let responce = [];

  for (let i = 0; i < req.body.length; i++) {
    req.body[i].addedBy = user.id;
    req.body[i].subjectType = subjectType;
    req.body[i].subject = object.id;
    let type = req.body[i].type;
    if (!type)
      return APIResponse.BadRequest(res, 'Type of question is required .');
    let newQuestion = await new models[type](req.body[i]).save();
    responce.push(newQuestion);
  }

  return APIResponse.Created(res, responce);
});
