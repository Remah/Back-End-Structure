const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');
const digitaloceanspaces = require('../../services/digitaloceanspaces');

module.exports = $baseCtrl(
  [{ name: 'image', maxCount: 1 }],
  digitaloceanspaces,
  async (req, res) => {
    const question = await models.question.findById(
      parseInt(req.params.id),
    );
    if (!question)
      return APIResponse.NotFound(res, 'No quesetion with that id');

    if (question.addedBy !== req.me.id)
      return APIResponse.Forbidden(
        res,
        'Dont allow to update this question',
      );

    delete req.body.subject;
    delete req.body.subjectType;
    delete req.body.type;
    delete req.body.addedBy;
    // check if user upload photo for question
    if (req.files && req.files['image']) {
      req.body.image = req.files['image'][0].location;
    }

    await question.set(req.body).save();

    return APIResponse.Ok(res, question);
  },
);
