const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');
const digitaloceanspaces = require('../../services/digitaloceanspaces');

module.exports = $baseCtrl(
  [{ name: 'image', maxCount: 1 }],
  digitaloceanspaces,
  async (req, res) => {
    let user = req.authenticatedUser;
    req.body.addedBy = user.id;

    let type = req.body.type;
    if (!type)
      return APIResponse.BadRequest(res, 'Type of question is required .');

    let routePath = req.route.path.split('/');
    let subjectType = routePath[1] === 'live' ? 'live' : 'course';

    const object = await models[subjectType].findById(
      parseInt(req.params.id),
    );
    if (!object)
      return APIResponse.NotFound(res, `No ${subjectType} with that id`);

    if (object.teacher !== req.me.id)
      return APIResponse.Forbidden(
        res,
        'Dont allow to add questions in this section',
      );

    // check if user upload photo for question
    if (req.files && req.files['image']) {
      req.body.image = req.files['image'][0].location;
    }

    req.body.subject = object.id;
    req.body.subjectType = subjectType;
    let newQuestion = await new models[type](req.body).save();

    return APIResponse.Created(res, newQuestion);
  },
);
