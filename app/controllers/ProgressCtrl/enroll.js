const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');

module.exports = $baseCtrl(async (req, res) => {
  let courseId = parseInt(req.params.id);
  const course = await models.course.findById(courseId);
  if (!course) return APIResponse.NotFound(res, 'No course with that id');
  let enroll = await models.courseProgress.findOne({ user: req.me.id, course: courseId });
  console.log('prev', enroll);
  if (!enroll) {
    let countLessons = await models.lesson.countDocuments({ course: course.id });
    enroll = await new models.courseProgress({
      user: req.me.id,
      course: courseId,
      numberOfCourseLessons: countLessons,
    }).save();
    console.log('New', enroll);
    course.numberOfEnrolledStudents++;
    await course.save();
  }
  return APIResponse.Ok(res, enroll);
});
