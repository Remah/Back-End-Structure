const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');

module.exports = $baseCtrl(async (req, res) => {
  let certificatesEarned = await models._progress.countDocuments({
    user: req.me.id,
    isCertificated: true,
  });
  let courses = await models.courseProgress.find({ user: req.me.id });
  let coursesCompleted = courses.filter(
    (course) => course.isCompleted === true,
  );
  let coursesInProgress = courses.length - coursesCompleted.length;

  let allPoints = await models.point.aggregate([
    { $match: {student: req.me.id}},
    { $group: { _id: '$student', points: { $sum: '$points' } } }
  ])


  return APIResponse.Ok(res, {
    certificatesEarned,
    coursesCompleted: coursesCompleted.length,
    coursesInProgress,
    points: allPoints[0] ? allPoints[0].points : 0
  });
});
