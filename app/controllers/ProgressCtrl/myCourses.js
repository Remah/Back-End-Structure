const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');

module.exports = $baseCtrl(async (req, res) => {
  let myCourses = await models.courseProgress.fetchAll(
    req.allowPagination,
    { user: req.me.id },
    {
      ...req.queryOptions,
      populate: [{path:'course',populate:[{path: 'teacher',select: 'username photo'},'discount']}, 'lessons']
    }
    )
  
  return APIResponse.Ok(res, myCourses);
});

/*
const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');

module.exports = $baseCtrl(async (req, res) => {
  let myCourses = await models.courseProgress.fetchAll(
    req.allowPagination,
    { user: req.me.id },
    {
      ...req.queryOptions,
      populate: [{path:'course',populate:[{path: 'teacher',select: 'username photo'},'discount']}, 'lessons']
    }
    )


  return APIResponse.Ok(res, myCourses);
});

*/