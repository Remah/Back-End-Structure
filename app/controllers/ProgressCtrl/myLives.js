const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');

module.exports = $baseCtrl(async (req, res) => {
  let myLives = await models.liveProgress.find({ user: req.me.id })
    .populate([{path:'live',populate:[{path: 'teacher',select: 'username photo'},'discount']}]);
  return APIResponse.Ok(res, myLives);
});

/*
const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');

module.exports = $baseCtrl(async (req, res) => {
  let myLives = await models.liveProgress.fetchAll(
    req.allowPagination,
    { user: req.me.id },
    {
      ...req.queryOptions,
      populate: [{path:'live',populate:[{path: 'teacher',select: 'username photo'},'discount']}]
    }
    )
  return APIResponse.Ok(res, myLives);
});

*/