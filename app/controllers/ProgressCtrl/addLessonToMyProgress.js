const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');

module.exports = $baseCtrl(async (req, res) => {
  const lesson = await models.lesson.findById(parseInt(req.params.id));
  if (!lesson) return APIResponse.NotFound(res, 'No lesson with that id');

  let progress = await models.courseProgress.findOne({ user: req.me.id, course: lesson.course });
  if (!progress) return APIResponse.BadRequest(res, 'Something is wrong');

  if (progress.lessons.indexOf(lesson.id) === -1) 
  {
    lesson.views++
    await lesson.save()
    progress.lessons.push(lesson.id);
    await progress.save();
  }
  return APIResponse.Ok(res, progress);
});
