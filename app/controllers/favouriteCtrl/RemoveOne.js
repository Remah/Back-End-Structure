const $baseCtrl = require("../$baseCtrl");
const models = require("../../models");
const { APIResponse } = require("../../utils");

module.exports = $baseCtrl(async (req, res) => {
  let course = await models.course.findById(req.params.id);
  if (!course) return APIResponse.NotFound(res, "No course with that id");

  if (req.me.favourites.indexOf(course.id) !== -1) {
    req.me.favourites.splice(req.me.favourites.indexOf(course.id), 1);
    await req.me.save();
  }
  let userPopulated = await models._user.populate(req.me, {
    path: "favourites",
    populate: ["category", {path:'teacher',select:'username photo'}],
  });

  return APIResponse.Ok(res, userPopulated.favourites);
});
