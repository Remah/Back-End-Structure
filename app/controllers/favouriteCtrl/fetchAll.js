const $baseCtrl = require("../$baseCtrl");
const models = require("../../models");
const { APIResponse } = require("../../utils");
const { escapeRegExp } = require("lodash");

module.exports = $baseCtrl(async (req, res) => {
 
  let courses = await models.course.fetchAll(
    req.allowPagination,
    {
      _id : {$in : req.me.favourites},
      ...(req.query.name && { $or: [{ nameAr: new RegExp(escapeRegExp(req.query.name), "i") }, { nameEn:  new RegExp(escapeRegExp(req.query.name), "i") }]})
    },
    {
      ...req.queryOptions,
      populate:[{path: "teacher" , select: 'username photo'}, "category","subCategory",'secondSubCategory','discount']
    }
  )
  return APIResponse.Ok(res,courses)
});


/*
const $baseCtrl = require("../$baseCtrl");
const models = require("../../models");
const { APIResponse } = require("../../utils");

module.exports = $baseCtrl(async (req, res) => {
  console.log(req.me.favourites)
  let userPopulated = await models._user.populate(req.me, {
    path: "favourites",
    populate: [{path: "teacher" , select: 'username photo'}, "category","subCategory",'secondSubCategory','discount'],
  });

  return APIResponse.Ok(res, userPopulated.favourites);
});

*/