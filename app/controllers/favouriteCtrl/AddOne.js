const $baseCtrl = require("../$baseCtrl");
const models = require("../../models");
const { APIResponse } = require("../../utils");

module.exports = $baseCtrl(async (req, res) => {
  const id = parseInt(req.params.id)
  if(isNaN(id)) return APIResponse.BadRequest(res)
  let course = await models.course.findById(id);
  if (!course) return APIResponse.NotFound(res, "No course with that id");

  req.me.lastCategoryFavourite = course.secondSubCategory
  if(req.me.recommended.indexOf(id) === -1){
    req.me.recommended.push(id)
  }
  if (req.me.favourites.indexOf(course.id) === -1) {
    req.me.favourites.push(course.id);
  }
  await req.me.save();
  let userPopulated = await models._user.populate(req.me, {
    path: "favourites",
    populate: ["category", {path:'teacher',select:'username photo'}],
  });

  return APIResponse.Ok(res, userPopulated.favourites);
});
