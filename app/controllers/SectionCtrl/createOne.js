const $baseCtrl = require("../$baseCtrl");
const models = require("../../models");
const { APIResponse } = require("../../utils");

module.exports = $baseCtrl(async (req, res) => {
  const course = await models.course.findById(req.params.id);
  if (!course) return APIResponse.NotFound(res, "No course with that id");
  req.body.course = course.id;
  const section = await new models.section(req.body).save();
  return APIResponse.Created(res, section);
});
