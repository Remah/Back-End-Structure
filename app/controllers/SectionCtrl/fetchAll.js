const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');

module.exports = $baseCtrl(async (req, res) => {
  const course = await models.course.findById(req.params.id);
  if (!course) return APIResponse.NotFound(res, 'No course with that id');
  let sections = await models.section.find({ course: course.id });
  sections = res.toJSON(sections);

  let numberOfLessons = await models.lesson.countDocuments({
    course: course.id,
  });

  // if not auth user
  if(!req.me){
    for (let i = 0; i < sections.length; i++) {
      let lessons = await models.lesson.find({ section: sections[i].id }).select('nameAr nameEn duration views');
      sections[i].lessons = lessons;
    }
    let response = {
      sections,
      totalHours: course.numberOfHours,
      numberOfLessons,
    };
    return APIResponse.Ok(res,response)
  } 

  // Dashboard for teacher
  if(req.me.role === 'teacher' && course.teacher === req.me.id)
  {
    for(let i = 0;i < sections.length;i++){
      let quiz = await models.courseQuiz.findOne({ section: sections[i].id, type: 'section' }).populate('questions.question');
      sections[i].quiz = quiz
    }
    return APIResponse.Ok(res,sections)
  }

  

  
  

  // check if user buy this course or not
  let progress = await models.courseProgress.findOne({user:req.me.id , course: course.id})
  
  // if auth user and didn't buy this course
  if(!progress){
    for (let i = 0; i < sections.length; i++) {
      let lessons = await models.lesson.find({ section: sections[i].id }).select('nameAr nameEn duration views');
      sections[i].lessons = lessons;
      sections[i].isPassPrevSection = false;
    }
    let response = {
      sections,
      totalHours: course.numberOfHours,
      numberOfLessons,
    };
    return APIResponse.Ok(res,response)
  }
  
  console.log('here')

  // if auth user and buy this course 
  let isPassPrevSection = true;
  for (let i = 0; i < sections.length; i++) {
    let lessons = await models.lesson.find({ section: sections[i].id });
    sections[i].lessons = lessons;
    sections[i].isPassPrevSection = isPassPrevSection;
    if (!isPassPrevSection) continue;
    let quiz = await models.courseQuiz.findOne({ section: sections[i].id, type: 'section' });
    console.log('quiz',quiz)
    if (!quiz) {
      isPassPrevSection = false;
      continue;
    }
    let lastSolutionDone = await models.solution
      .find({ student: req.authenticatedUser.id, quiz: quiz.id, status: 'done' })
      .sort({ _id: -1 })
      .limit(1);
    lastSolutionDone = lastSolutionDone[0];
    console.log('solution',lastSolutionDone)
    if (lastSolutionDone)
      isPassPrevSection = parseInt((lastSolutionDone.mark / quiz.points) * 100) >= 50;
    else isPassPrevSection = false;
  }
   let response = {
    sections,
    totalHours: course.numberOfHours,
    numberOfLessons,
  };
  return APIResponse.Ok(res, response);
});
