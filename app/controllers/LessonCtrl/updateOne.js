const $baseCtrl = require("../$baseCtrl");
const models = require("../../models");
const { APIResponse } = require("../../utils");
const digitaloceanspaces = require("../../services/digitaloceanspaces");

module.exports = $baseCtrl(
  [
    { name: "pdf", maxCount: 1 },
    { name: "video", maxCount: 1 }
  ],
  digitaloceanspaces,
  async (req, res) => {
    const id = parseInt(req.params.id);
    if (isNaN(id)) return APIResponse.NotFound(res);

    const lesson = await models.lesson.findById(id);
    if (!lesson) return APIResponse.NotFound(res, "No lesson with that id");

    if (req.files && req.files["pdf"]) {
      req.body.pdf = req.files["pdf"][0].location;
    }

    if (req.files && req.files["video"]) {
      if(!req.me.isAllowedUpdateVideos)
        return APIResponse.Forbidden(res,'Dont allow to update video or delete until admin allow to you')
      req.body.video = req.files["video"][0].location;
    }

    delete req.body.section;
    delete req.body.course;
    await lesson.set(req.body).save();
    return APIResponse.Ok(res, lesson);
  }
);
