const $baseCtrl = require("../$baseCtrl");
const models = require("../../models");
const { APIResponse } = require("../../utils");

module.exports = $baseCtrl(async (req, res) => {
  const id = parseInt(req.params.id);
  if (isNaN(id)) return APIResponse.NotFound(res);

  let lesson = await models.lesson.findById(id).populate(['section','course']);
  if (!lesson) return APIResponse.NotFound(res, "No lesson with that id");

  // Check if authorized 
  if(req.me.role === 'student'){
    let progress = await models.courseProgress.findOne({user: req.me.id, course: lesson.section.course})
    if(!progress) return APIResponse.Forbidden(res,'You dont allow to view lessons of this section')
  }

  return APIResponse.Ok(res, lesson);
});
