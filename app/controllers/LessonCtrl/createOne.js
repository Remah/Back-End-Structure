const $baseCtrl = require("../$baseCtrl");
const models = require("../../models");
const { APIResponse } = require("../../utils");
const digitaloceanspaces = require("../../services/digitaloceanspaces");

module.exports = $baseCtrl(
  [
    { name: "pdf", maxCount: 1 },
    { name: "video", maxCount: 1 }
  ],
  digitaloceanspaces,
  async (req, res) => {
    const section = await models.section.findById(req.params.id);
    if (!section) return APIResponse.NotFound(res, "No section with that id");

    req.body.section = section.id;
    req.body.course = section.course;

    if (req.files && req.files["pdf"]) {
      req.body.pdf = req.files["pdf"][0].location;
    } else return APIResponse.BadRequest(res, "No pdf files uploaded");

    if (req.files && req.files["video"]) {
      req.body.video = req.files["video"][0].location;
    } else return APIResponse.BadRequest(res, "No video uploaded");

    let newLesson = await models.lesson(req.body).save();
    return APIResponse.Created(res, newLesson);
  }
);
