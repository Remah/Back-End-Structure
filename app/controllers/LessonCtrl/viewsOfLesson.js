const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');

module.exports = $baseCtrl(async (req, res) => {
  const id = parseInt(req.params.id);
  if (isNaN(id)) return APIResponse.NotFound(res);

  let lesson = await models.lesson.findById(id);
  if (!lesson) return APIResponse.NotFound(res, 'No lesson with that id');
  let views = await models.courseProgress.countDocuments({ lessons: id });
  return APIResponse.Ok(res, { views });
});
