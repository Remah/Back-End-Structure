const $baseCtrl = require("../$baseCtrl");
const models = require("../../models");
const { APIResponse } = require("../../utils");

module.exports = $baseCtrl(async (req, res) => {
  const section = await models.section.findById(req.params.id);
  if (!section) return APIResponse.NotFound(res, "No section with that id");

  // Check if authorized 
  if(req.me.role === 'student'){
    let progress = await models.courseProgress.findOne({user: req.me.id, course: section.course})
    if(!progress) return APIResponse.Forbidden(res,'You dont allow to view lessons of this section')
  }


  const lessons = await models.lesson
    .find({ section: section.id })
    .populate(['section','course']);
  return APIResponse.Ok(res, lessons);
});
