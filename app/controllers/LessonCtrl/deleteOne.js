const $baseCtrl = require("../$baseCtrl");
const models = require("../../models");
const { APIResponse } = require("../../utils");

module.exports = $baseCtrl(async (req, res) => {
  const id = parseInt(req.params.id);
  if (isNaN(id)) return APIResponse.NotFound(res);

  const lesson = await models.lesson.findById(id);

  if (!lesson) return APIResponse.NotFound(res, "No lesson with that id");

  if(!req.me.isAllowedUpdateVideos)
        return APIResponse.Forbidden(res,'Dont allow to update video or delete until admin allow to you')

  await lesson.delete();

  return APIResponse.NoContent(res);
});
