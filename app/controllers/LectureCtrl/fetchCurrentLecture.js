const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');

module.exports = $baseCtrl(async (req, res) => {
  const live = await models.live
    .findById(parseInt(req.params.id))
    .populate({ path: 'lecture', populate: 'live' });
  if (!live) return APIResponse.NotFound(res, 'No live with that id');

  // check if authorized
  if(req.me.role === 'student'){
    let progress = await models.liveProgress.findOne({user: req.me.id, live: live.id})
    if(!progress) return APIResponse.Forbidden(res,'You dont allow to view lecture of this live')
  }

  return APIResponse.Ok(res, live.lecture);
});
