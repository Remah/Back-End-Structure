const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');

module.exports = $baseCtrl(async (req, res) => {
  const id = parseInt(req.params.id)
  if(isNaN(id)) return APIResponse.NotFound(res,'Id is not a number')
  const lecture = await models.lecture.findById(id)
  if(!lecture) return APIResponse.NotFound(res,'No Lecture with that id')

  if(lecture.users.indexOf(req.me.id) === -1)
  {
      lecture.users.push(req.me.id)
      await lecture.save()
  }
  return APIResponse.Ok(res,lecture)
});
