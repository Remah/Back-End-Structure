const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');

module.exports = $baseCtrl(async (req, res) => {
  const id = parseInt(req.params.id)
  if(isNaN(id)) return APIResponse.NotFound(res,'Id is not a number')
  const lecture = await models.lecture.findById(id).populate([{path:'users',select:'username email phone photo'},'live'])
  if(!lecture) return APIResponse.NotFound(res,'No Lecture with that id')

  if(lecture.live.teacher !== req.me.id)
    return APIResponse.Forbidden(res,'Dont allow to see attendees of this lecture')
  
  return APIResponse.Ok(res,lecture.users)
});
