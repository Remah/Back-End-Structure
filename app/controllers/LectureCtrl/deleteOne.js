const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');

module.exports = $baseCtrl(async (req, res) => {
  const lecture = await models.lecture
    .findById(req.params.id)
    .populate('live');
  if (!lecture)
    return APIResponse.NotFound(res, 'No lecture with that id');
  if (lecture.live.teacher !== req.me.id)
    return APIResponse.Forbidden(res, 'Dont allow to Delete this lecture');

  await lecture.delete();
  return APIResponse.NoContent(res);
});
