const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');

module.exports = $baseCtrl(async (req, res) => {
  const live = await models.live.findById(parseInt(req.params.id));
  if (!live) return APIResponse.NotFound(res, 'No live with that id');
  if (live.teacher !== req.me.id)
    return APIResponse.Forbidden(
      res,
      'Dont allow to add lecture in this Live course',
    );
  req.body.live = live.id;
  delete req.body.videoUrl;
  const lecture = await new models.lecture(req.body).save();

  await live.set({ lecture: lecture.id }).save();
  
   APIResponse.Created(res, lecture);

  if (req.body.liveUrl) {
    // send notification to all  students in this live course
    const initiator = req.authenticatedUser;
    const receivers = await models.liveProgress.find({live: live.id}).populate('user')
    for (let i = 0; i < receivers.length; i++) {
      let receiver = receivers[i].user;
      let title = '📝 تم إضافة بث مباشر ';
      let body = `📝 تم إضافة بث مباشر على  ${lecture.title}`;
      const notification = await models
        .notification({
          title: title,
          body: body,
          initiator: initiator.id,
          receiver: receiver.id,
          subjectType: 'lecture',
          subject: lecture.id,
        })
        .save();
      await receiver.sendNotification(notification.toFirebaseNotification());
  }
}
});
