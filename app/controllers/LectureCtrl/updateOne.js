const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');
const digitaloceanspaces = require("../../services/digitaloceanspaces");

module.exports = $baseCtrl(
  [
    { name: "videoUrl", maxCount: 1 }
  ],
  digitaloceanspaces,
  async (req, res) => {
  const lecture = await models.lecture
    .findById(req.params.id)
    .populate('live');
  if (!lecture)
    return APIResponse.NotFound(res, 'No lecture with that id');
  if (lecture.live.teacher !== req.me.id)
    return APIResponse.Forbidden(res, 'Dont allow to update this lecture');

  delete req.body.live;

  if (req.files && req.files["videoUrl"]) {
    req.body.videoUrl = req.files["videoUrl"][0].location;
  }

  if (req.body.videoUrl) {
    // delete live link
    req.body.liveUrl = undefined;
    // delete live lecture from live course document
    let liveCourse = await models.live.findById(lecture.live.id);
    if (liveCourse) await liveCourse.set({ lecture: undefined }).save();
  }

  
  await lecture.set(req.body).save();
  APIResponse.Ok(res, lecture);
    if (req.body.liveUrl) {
      // send notification to all  students in this live course
      const initiator = req.authenticatedUser;
      const receivers = await models.liveProgress.find({live: lecture.live.id}).populate('user')
      for (let i = 0; i < receivers.length; i++) {
        let receiver = receivers[i].user;
        let title = '📝 تم إضافة بث مباشر ';
        let body = `📝 تم إضافة بث مباشر على  ${lecture.title}`;
        const notification = await models
          .notification({
            title: title,
            body: body,
            initiator: initiator.id,
            receiver: receiver.id,
            subjectType: 'lecture',
            subject: lecture.id,
          })
          .save();
        await receiver.sendNotification(notification.toFirebaseNotification());
    }
  }
});
