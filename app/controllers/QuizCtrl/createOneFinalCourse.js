const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');

module.exports = $baseCtrl(async (req, res) => {
  const course = await models.course.findById(parseInt(req.params.id));
  if (!course) return APIResponse.NotFound(res, 'No course with that id');

  if (course.teacher !== req.me.id)
    return APIResponse.Forbidden(res, 'You dont allow to add Final Exam to this course');

  const prevFinalQuiz = await models.courseQuiz.findOne({course:course.id,type:'final'})
  if(prevFinalQuiz) return APIResponse.Forbidden(res,'Not allowed to add 2 final quizes to same course')

  delete req.body.section;
  req.body.course = course.id;
  req.body.type = 'final';
  let quiz = await new models.courseQuiz(req.body).save();
  return APIResponse.Created(res, quiz);
});
