const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');
module.exports = $baseCtrl(async (req, res) => {
  const course = await models.course.findById(parseInt(req.params.id));
  if (!course) return APIResponse.NotFound(res, 'No course with that id');
  let filter = {
    course: course.id,
    type: 'final',
    ...(req.me.role === 'student' && {
      availability: true,
    }),
  };
  let quiz = await models.courseQuiz.findOne(filter);
  if (quiz && req.me.role === 'student') {
    let lastSolutionDone = await models.solution
      .find({
        student: req.authenticatedUser.id,
        quiz: quiz.id,
        status: 'done',
      })
      .sort({ _id: -1 })
      .limit(1);
    quiz = res.toJSON(quiz, {
      append: {
        mark: lastSolutionDone[0] ? parseInt(lastSolutionDone[0].mark / quiz.points * 100) : undefined,
      },
    });
  }

  return APIResponse.Created(res, quiz);
});
