const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');

module.exports = $baseCtrl(async (req, res) => {
    const id = parseInt(req.params.id)
    if(isNaN(id)) return APIResponse.NotFound(res)
    let quiz = await models._quiz
    .findById(id)
    .populate(['live', 'course']);
  if (!quiz) return APIResponse.NotFound(res, 'No quiz with that id');

  if (quiz.kind === 'courseQuiz') {
    if (quiz.course.teacher !== req.me.id)
      return APIResponse.Forbidden(
        res,
        'You dont allow to delete this exam',
      );
  } else {
    if (quiz.live.teacher !== req.me.id)
      return APIResponse.Forbidden(
        res,
        'You dont allow to delete this exam',
      );
  }

  await models.solution.deleteMany({quiz:quiz.id})
  await quiz.delete()
  return APIResponse.NoContent(res)
});
