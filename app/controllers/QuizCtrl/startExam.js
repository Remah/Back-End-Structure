const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');

module.exports = $baseCtrl(async (req, res) => {
  const quiz = await models._quiz.findById(parseInt(req.params.id));
  if (!quiz) return APIResponse.NotFound(res, 'No quiz with that id');

  // here check first if type === 'final' , then check if pass all prev section Exams , Mobile should prevent him from this step
  if (quiz.type === 'final' && quiz.kind === 'courseQuiz') {
    let sections = await models.section.find({ course: quiz.course });
    let isPassPrevSection = true;
    for (let i = 0; i < sections.length; i++) {
      if (!isPassPrevSection) break;
      let courseQuiz = await models.courseQuiz.findOne({
        section: sections[i].id,
        type: 'section',
      });
      if (!courseQuiz) {
        isPassPrevSection = false;
        break;
      }
      let lastSolutionDone = await models.solution
        .find({
          student: req.authenticatedUser.id,
          quiz: courseQuiz.id,
          status: 'done',
        })
        .sort({ _id: -1 })
        .limit(1);
      lastSolutionDone = lastSolutionDone[0];
      if (lastSolutionDone)
        isPassPrevSection =
          parseInt((lastSolutionDone.mark / courseQuiz.points) * 100) >=
          50;
      else isPassPrevSection = false;
    }
    if (!isPassPrevSection)
      return APIResponse.Forbidden(
        res,
        'You Dont allow to solve final exam until solve all prev section Exams',
      );
  }

  // check if timer expire for liveQuiz
  if (quiz.kind === 'liveQuiz' && quiz.isLive === false) {
    return APIResponse.Forbidden(res, 'Timer finished');
  }

  let pastSolution = await models.solution
    .find({
      student: req.authenticatedUser.id,
      quiz: quiz.id,
      status: 'done',
    })
    .sort({ _id: -1 })
    .limit(1);
  pastSolution = pastSolution[0]; // if array is empty , pastSolution[0] will be undefined
  if (pastSolution) {
    // here check if quiz.type === 'final' , return 403 regardless his mark
    if (quiz.kind === 'liveQuiz')
      return APIResponse.Forbidden(
        res,
        'You cant solve live quiz exam twice',
      );
    // only quiz section allow to solve more than one , if not pass 50%
    if (quiz.type === 'final')
      return APIResponse.Forbidden(
        res,
        'You solve this final exam before',
      );
    let isPass = parseInt((pastSolution.mark / quiz.points) * 100) >= 50;
    if (isPass)
      return APIResponse.Forbidden(res, 'You have pass this quiz before');
  }

  let currentSolution = await models.solution.findOne({
    student: req.authenticatedUser.id,
    quiz: quiz.id,
    status: 'solving',
  });

  if (currentSolution)
    return APIResponse.Ok(res, {
      msg: 'You have already start this exam',
    });

  let newSolution = await new models.solution({
    student: req.authenticatedUser.id,
    quiz: quiz.id,
    status: 'solving',
  }).save();
  return APIResponse.NoContent(res);
});
