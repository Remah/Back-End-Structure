const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');

module.exports = $baseCtrl(async (req, res) => {
  const section = await models.section.findById(parseInt(req.params.id)).populate('course');
  if (!section) return APIResponse.NotFound(res, 'No section with that id');

  if (section.course.teacher !== req.me.id)
    return APIResponse.Forbidden(res, 'You dont allow to add quiz to this lesson');

  const prevSectionQuiz = await models.courseQuiz.findOne({section:section.id,type:'section'})
  if(prevSectionQuiz) return APIResponse.Forbidden(res,'Not allowed to add 2 quizes to same section')

  req.body.section = section.id;
  req.body.course = section.course.id;
  req.body.type = 'section';
  let quiz = await new models.courseQuiz(req.body).save();
  return APIResponse.Created(res, quiz);
});
