const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');

module.exports = $baseCtrl(async (req, res) => {
  let quiz = await models._quiz
    .findById(parseInt(req.params.id))
    .populate(['course', 'live']);
  if (!quiz) return APIResponse.NotFound(res, 'No quiz with that id');

  let allowed =
    quiz.kind === 'courseQuiz'
      ? quiz.course.teacher === req.me.id
      : quiz.live.teacher === req.me.id;
  if (!allowed)
    return APIResponse.Forbidden(
      res,
      'You cant add questions to this quiz',
    );
  const user = req.authenticatedUser;

  for (let i = 0; i < req.body.length; i++) {
    req.body[i].addedBy = user.id;
    req.body[i].subjectType =
      quiz.kind === 'courseQuiz' ? 'course' : 'live';
    req.body[i].subject =
      quiz.kind === 'courseQuiz' ? quiz.course.id : quiz.live.id;
    let type = req.body[i].type;
    if (!type)
      return APIResponse.BadRequest(res, 'Type of question is required .');
    let newQuestion = await new models[type](req.body[i]).save();
    quiz.questions.push({
      question: newQuestion.id,
      point: parseInt(req.body[i].point),
    });
  }

  await quiz.save();

  let response = await models._quiz.populate(quiz, [
    'questions.question',
    'lesson',
    'course',
    'live',
  ]);

  return APIResponse.Created(res, response);
});
