const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');

module.exports = $baseCtrl(async (req, res) => {
  const live = await models.live.findById(parseInt(req.params.id));
  if (!live) return APIResponse.NotFound(res, 'No live with that id');

  if (live.teacher !== req.me.id)
    return APIResponse.Forbidden(
      res,
      'Dont allow to add one to this Live course',
    );
  
  if(req.body.type === 'final'){
    const prevLiveFinalQuiz = await models.liveQuiz.findOne({live:live.id,type:'final'})
    if(prevLiveFinalQuiz) return APIResponse.Forbidden(res,'Dont allow to add 2 final quizes to same live course')
  }

  req.body.live = live.id;
  const quiz = await new models.liveQuiz(req.body).save();
  return APIResponse.Created(res, quiz);
});
