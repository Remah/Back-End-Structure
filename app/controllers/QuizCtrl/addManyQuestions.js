const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');

module.exports = $baseCtrl(async (req, res) => {
  let quiz = await models._quiz
    .findById(parseInt(req.params.id))
    .populate(['course', 'live']);
  if (!quiz) return APIResponse.NotFound(res, 'No quiz with that id');
  let allowed =
    quiz.kind === 'courseQuiz'
      ? quiz.course.teacher === req.me.id
      : quiz.live.teacher === req.me.id;
  if (!allowed)
    return APIResponse.Forbidden(
      res,
      'You cant add questions to this quiz',
    );

  let quizObj = {};
  quiz.questions.forEach((q) => {
    quizObj[q.question] = q;
  });

  for (let i = 0; i < req.body.length; i++) {
    let question = await models.question.findById(req.body[i].question);

    if (!question)
      return APIResponse.NotFound(res, 'No question with that id');

    if (quizObj[req.body[i].question] !== undefined)
      return APIResponse.Forbidden(
        res,
        `You already add this question ${req.body[i].question} to quiz`,
      );

    quiz.questions.push({
      question: req.body[i].question,
      point: parseInt(req.body[i].point),
    });
  }

  await quiz.save();

  let response = await models._quiz.populate(quiz, [
    'questions.question',
    'lesson',
    'course',
  ]);

  return APIResponse.Created(res, response);
});
