const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');
const _ = require('lodash');

module.exports = $baseCtrl(async (req, res) => {
  let quiz = await models._quiz.findById(parseInt(req.params.id));
  if (!quiz) return APIResponse.NotFound(res, 'No quiz with that id');

  // check if timer expire for liveQuiz
  if (quiz.kind === 'liveQuiz' && quiz.isLive === false) {
    return APIResponse.Forbidden(res, 'Timer finished');
  }

  let solution = await models.solution.findOne({
    student: req.authenticatedUser.id,
    quiz: quiz.id,
    status: 'solving',
  });
  if (!solution)
    return APIResponse.NotFound(
      res,
      'No solving solution to that student to submit answers',
    );

  let question = await models.question.findById(req.body.question);
  if (!question)
    return APIResponse.NotFound(res, 'No question with that id');

  let responce = {};
  let key = _.findKey(
    solution.questions,
    _.matchesProperty('question', parseInt(req.body.question)),
  );
  if (key !== undefined) {
    solution.questions[key].answer = req.body.answer;
    responce = solution.questions[key];
  } else {
    solution.questions.push({
      question: req.body.question,
      type: question.type,
      answer: req.body.answer,
    });
    responce = solution.questions[solution.questions.length - 1];
  }

  await solution.save();

  return APIResponse.Ok(res, responce);
});
