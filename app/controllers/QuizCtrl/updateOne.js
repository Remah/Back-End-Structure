const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');
// [TODO] handle if update dawra exams
module.exports = $baseCtrl(async (req, res) => {
  let quiz = await models._quiz
    .findById(parseInt(req.params.id))
    .populate(['live', 'course']);
  if (!quiz) return APIResponse.NotFound(res, 'No quiz with that id');

  if (quiz.kind === 'courseQuiz') {
    if (quiz.course.teacher !== req.me.id)
      return APIResponse.Forbidden(
        res,
        'You dont allow to edit this exam',
      );
  } else {
    if (quiz.live.teacher !== req.me.id)
      return APIResponse.Forbidden(
        res,
        'You dont allow to edit this exam',
      );
  }

  delete req.body.section;
  delete req.body.course;
  delete req.body.live;
  delete req.body.questions;
  delete req.body.type;

  await quiz.set(req.body).save();

  return APIResponse.Ok(res, quiz);
});
