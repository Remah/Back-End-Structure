const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');

module.exports = $baseCtrl(async (req, res) => {
  const section = await models.section
    .findById(parseInt(req.params.id))
    .populate('course');
  if (!section)
    return APIResponse.NotFound(res, 'No section with that id');

  let filter = {
    section: section.id,
    type: 'section',
    ...(req.me.role === 'student' && {
      availability: true,
    }),
  };
  let quiz = await models.courseQuiz.findOne(filter);

  if (quiz && req.me.role === 'student') {
    let lastSolutionDone = await models.solution
      .find({
        student: req.authenticatedUser.id,
        quiz: quiz.id,
        status: 'done',
      })
      .sort({ _id: -1 })
      .limit(1);
    quiz = res.toJSON(quiz, {
      append: {
        mark: lastSolutionDone[0] ? parseInt(lastSolutionDone[0].mark / quiz.points * 100) : undefined,
      },
    });
  }

  return APIResponse.Created(res, quiz);
});
