const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');

module.exports = $baseCtrl(async (req, res) => {
  const live = await models.live.findById(parseInt(req.params.id));
  if (!live) return APIResponse.NotFound(res, 'No Live with that id');

  const filter = {
    ...req.queryFilter,
    ...(req.me.role === 'student' && { availability: true }),
    live: live.id,
  };

  let quizes = await models.liveQuiz.find(filter);
  if (req.me.role === 'student') {
    for (let i = 0; i < quizes.length; i++) {
      let lastSolutionDone = await models.solution
        .find({
          student: req.authenticatedUser.id,
          quiz: quizes[i].id,
          status: 'done',
        })
        .sort({ _id: -1 })
        .limit(1);
      quizes[i] = res.toJSON(quizes[i], {
        append: {
          mark: lastSolutionDone[0] ? parseInt(lastSolutionDone[0].mark / quizes[i].points * 100): undefined,
        },
      });
    }
  }

  return APIResponse.Ok(res, quizes);
});


/*
const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');

module.exports = $baseCtrl(async (req, res) => {
  const live = await models.live.findById(parseInt(req.params.id));
  if (!live) return APIResponse.NotFound(res, 'No Live with that id');

  const filter = {
    ...req.queryFilter,
    ...(req.me.role === 'student' && { availability: true }),
    live: live.id,
  };

  let quizesAll = await models.liveQuiz.fetchAll(
    req.allowPagination,
    filter,
    req.queryOptions
  );
  let quizes = req.allowPagination ? quizesAll.docs : quizesAll
  if (req.me.role === 'student') {
    for (let i = 0; i < quizes.length; i++) {
      let lastSolutionDone = await models.solution
        .find({
          student: req.authenticatedUser.id,
          quiz: quizes[i].id,
          status: 'done',
        })
        .sort({ _id: -1 })
        .limit(1);
      quizes[i] = res.toJSON(quizes[i], {
        append: {
          mark: lastSolutionDone[0] ? parseInt(lastSolutionDone[0].mark / quizes[i].points * 100): undefined,
        },
      });
    }
  }

  if(req.allowPagination){
    quizesAll.docs = quizes
    return APIResponse.Ok(res,quizesAll)
  }

  return APIResponse.Ok(res, quizes);
});
*/