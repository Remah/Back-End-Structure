const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');
// [TODO] give point depend on mark
module.exports = $baseCtrl(async (req, res) => {
  let quiz = await models._quiz
    .findById(parseInt(req.params.id))
    .populate(['questions.question', 'lesson', 'course', 'live']);
  if (!quiz) return APIResponse.NotFound(res, 'No quiz with that id');
  let solution = await models.solution
    .findOne({
      student: req.authenticatedUser.id,
      quiz: quiz.id,
      status: 'solving',
    })
    .populate('questions.question');
  if (!solution)
    return APIResponse.NotFound(
      res,
      'NO solution to that student of this exam',
    );

  await solution.turnToChecking(quiz);
  let mark = parseInt((solution.mark / quiz.points) * 100);

  if (mark >= 50) {
    let point = await new models.point({
      student: req.me.id,
      points: solution.mark,
      category:
        quiz.kind === 'courseQuiz'
          ? quiz.course.category
          : quiz.live.category,
    }).save();
    console.log('new Point', point);
  }

  APIResponse.Ok(res, { mark: parseInt(mark) });

  if (quiz.type === 'final' && quiz.kind === 'courseQuiz') {
    let quizes = await models.courseQuiz.find({
      course: quiz.course.id,
      type: 'section',
    });
    let totalMarks = 0;
    let totalPoints = 0;
    for (let i = 0; i < quizes.length; i++) {
      console.log('quiz', quizes[i]);
      let prevSolution = await models.solution
        .find({ quiz: quizes[i].id, student: req.me.id, status: 'done' })
        .sort({ _id: -1 })
        .limit(1);
      prevSolution = prevSolution[0];
      console.log('solution', prevSolution);
      totalMarks += prevSolution.mark;
      totalPoints += quizes[i].points;
    }
    console.log('totalMarks', totalMarks);
    console.log('totalPoints', totalPoints);
    let final_mark =
      parseInt((totalMarks / totalPoints) * 50) + parseInt(mark / 2);
    console.log(final_mark);
    if (final_mark >= quiz.course.passing_percentage) {
      let progress = await models.courseProgress.findOne({
        course: quiz.course.id,
        user: req.me.id,
      });
      console.log('progress', progress);
      if (progress) {
        progress.isCertificated = true;
        await progress.save();
        console.log('CONGRATULATIONS');
      }
    }
  }

  if (quiz.type === 'final' && quiz.kind === 'liveQuiz') {
    let quizes = await models.liveQuiz.find({
      live: quiz.live.id,
      type: 'mid',
    });
    let totalMarks = 0;
    let totalPoints = 0;
    for (let i = 0; i < quizes.length; i++) {
      console.log('quiz', quizes[i]);
      let prevSolution = await models.solution
        .find({ quiz: quizes[i].id, student: req.me.id, status: 'done' })
        .sort({ _id: -1 })
        .limit(1);
      prevSolution = prevSolution[0];
      if(!prevSolution) continue // because in live quizes , student maybe not solve all quizes
      console.log('solution', prevSolution);
      totalMarks += prevSolution.mark;
      totalPoints += quizes[i].points;
    }
    console.log('totalMarks', totalMarks);
    console.log('totalPoints', totalPoints);
    let final_mark =
      parseInt((totalMarks / totalPoints) * 50) + parseInt(mark / 2);
    console.log('final mark', final_mark);
    console.log('passing percentage', quiz.live.passing_percentage);
    if (final_mark >= quiz.live.passing_percentage) {
      let progress = await new models.liveProgress({
        live: quiz.live.id,
        user: req.me.id,
        isCertificated: true,
      }).save();
      console.log('CONGRATULATIONS');
    }
  }
});
