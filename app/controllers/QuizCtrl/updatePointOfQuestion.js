const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');
const _ = require('lodash');

module.exports = $baseCtrl(async (req, res) => {
  let quiz = await models._quiz
    .findById(parseInt(req.params.eid))
    .populate('course');
  if (!quiz) return APIResponse.NotFound(res, 'No quiz with that id');
  let allowed =
    quiz.kind === 'courseQuiz'
      ? quiz.course.teacher === req.me.id
      : quiz.live.teacher === req.me.id;
  if (!allowed)
    return APIResponse.Forbidden(
      res,
      'You cant Update questions in this quiz',
    );

  if (req.body.point === undefined)
    return APIResponse.BadRequest(res, 'Must provide point in body');

  let key = _.findKey(
    quiz.questions,
    _.matchesProperty('question', parseInt(req.params.qid)),
  );
  if (key === undefined)
    return APIResponse.NotFound(res, 'No question with that id');
  quiz.questions[key].point = parseInt(req.body.point);
  await quiz.save();

  let quizResponce = await models._quiz.populate(quiz, [
    'questions.question',
    'section',
    'course',
  ]);
  return APIResponse.Ok(res, quizResponce);
});
