const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');
const moment = require('moment');

module.exports = $baseCtrl(async (req, res) => {
  let quiz = await models._quiz
    .findById(parseInt(req.params.id))
    .populate(['questions.question', 'section', 'course', 'live']);
  if (!quiz) return APIResponse.NotFound(res, 'No quiz with that id');
  // teacher & admin area
  if (req.me.role !== 'student') {
    return APIResponse.Ok(res, quiz);
  }

  // Check if authorized 
  if(quiz.kind === 'courseQuiz'){
    const filter = {
      user: req.me.id,
       course: quiz.type === 'section' ? quiz.section.course : quiz.course.id
      }
    let progress = await models.courseProgress.findOne(filter)
    if(!progress) return APIResponse.Forbidden(res,'You dont allow to view quiz of this course')

  } else {

    let progress = await models.liveProgress.findOne({user: req.me.id, live: quiz.live.id})
    if(!progress) return APIResponse.Forbidden(res,'You dont allow to view quiz of this live')
  }

  
  // student area => we have three cases :

  // first case : fetch Quiz to solve it
  let currentSolution = await models.solution
    .findOne({
      student: req.authenticatedUser.id,
      quiz: quiz.id,
      status: 'solving',
    })
    .populate('questions.question');
  if (currentSolution) {
    // check if timer expire for liveQuiz
  if (quiz.kind === 'liveQuiz' && quiz.isLive === false) {
    return APIResponse.Forbidden(res, 'Timer finished');
  }
    let solutionObj = {};
    currentSolution.questions.forEach((q) => {
      // if (!q.question) return; // if teacher delete question later , then when populate question will be null
      solutionObj[q.question.id] = q;
    });
    for (let i = 0; i < quiz.questions.length; i++) {
      let qId = quiz.questions[i].question.id;
      quiz.questions[i] = quiz.questions[i].toJSON();
      quiz.questions[i].answer =
        solutionObj[qId] === undefined ? null : solutionObj[qId].answer;
        quiz.questions[i].question.modelAnswer = undefined
    }
    return APIResponse.Ok(res, quiz);
  }

  // second case : fetch Quiz to see model Answers & his own answers
  let pastSolution = await models.solution
    .find({
      student: req.authenticatedUser.id,
      quiz: quiz.id,
      status: 'done',
    })
    .populate('questions.question')
    .sort({ _id: -1 })
    .limit(1);
  // get last solution of this student
  pastSolution = pastSolution[0];
  if (pastSolution) {
    // check if last solution on section quiz pass 50% or not
    if (quiz.type === 'section' && quiz.kind === 'courseQuiz') {
      let isPass = parseInt((pastSolution.mark / quiz.points) * 100) >= 50;
      if (!isPass)
        return APIResponse.Forbidden(
          res,
          'You have pass this quiz before',
        );
    }
    let solutionObj = {};
    pastSolution.questions.forEach((q) => {
      // if (!q.question) return;
      solutionObj[q.question.id] = q;
    });
    quiz = quiz.toJSON()
    for (let i = 0; i < quiz.questions.length; i++) {
      let qId = quiz.questions[i].question.id;
      quiz.questions[i] = quiz.questions[i].toJSON();
      quiz.questions[i].answer =
        solutionObj[qId] === undefined ? null : solutionObj[qId].answer;
      quiz.questions[i].mark =
        solutionObj[qId] === undefined ? 0 : solutionObj[qId].mark;
    }
    quiz.totalMarks = pastSolution.mark
    quiz.mark = parseInt(pastSolution.mark / quiz.points * 100)
    return APIResponse.Ok(res, quiz);
  }

  // Third case : If student does not have any solution
  return APIResponse.Forbidden(res, 'You have to Start Exam first please');
});
