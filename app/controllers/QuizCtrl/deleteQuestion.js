const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');
const _ = require('lodash');

module.exports = $baseCtrl(async (req, res) => {
  let quiz = await models._quiz
    .findById(parseInt(req.params.eid))
    .populate(['live', 'course']);
  if (!quiz) return APIResponse.NotFound(res, 'No quiz with that id');
  let allowed =
    quiz.kind === 'courseQuiz'
      ? quiz.course.teacher === req.me.id
      : quiz.live.teacher === req.me.id;
  if (!allowed)
    return APIResponse.Forbidden(
      res,
      'You cant delete questions from this quiz',
    );

  let key = _.findKey(
    quiz.questions,
    _.matchesProperty('question', parseInt(req.params.qid)),
  );
  if (key === undefined)
    return APIResponse.NotFound(res, 'No question with that id');
  quiz.questions.splice(key, 1);
  await quiz.save();

  let quizResponce = await models._quiz.populate(quiz, [
    'questions.question',
    'lesson',
    'course',
  ]);
  return APIResponse.Ok(res, quizResponce);
});
