const $baseCtrl = require('../$baseCtrl');
const { APIResponse } = require('../../utils');
const models = require('../../models');

module.exports = $baseCtrl(async (req, res) => {
    const course = await models.course.findById(parseInt(req.params.id))
    if(!course) return APIResponse.NotFound(res,'No Course With that id')

    if(course.teacher !== req.me.id && req.me.role !== 'admin')
        return APIResponse.Forbidden(res,'Dont allow to view these statistics')

    const progress = await models.courseProgress.fetchAll(
        req.allowPagination,
        {course: course.id},
        {
            ...req.queryOptions,
            populate: [{path: 'user', select:'username email photo phone'}]
        }
    )
    return APIResponse.Ok(res,progress)

});
