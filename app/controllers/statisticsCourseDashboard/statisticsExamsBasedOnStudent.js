const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');

module.exports = $baseCtrl(async (req, res) => {
  const cid = parseInt(req.params.cid)
  if(isNaN(cid)) return APIResponse.BadRequest(res)
  const course = await models.course.findById(cid)
  if(!course) return APIResponse.NotFound(res,'No Course with that id')


  if(course.teacher !== req.me.id && req.me.role !== 'admin')
    return APIResponse.Forbidden(res,'Dont allow to view these statistics')

  const sid = parseInt(req.params.sid)
  if(isNaN(sid)) return APIResponse.BadRequest(res)

  let quizes = await models.courseQuiz.find({course: cid})
  let quizesIds = quizes.map(quiz => quiz.id)

  const solutions = await models.solution.aggregate([
    { $match: { quiz: {$in : quizesIds}, status: 'done' , student: sid} },
    {
        $group: {
          _id: '$quiz',
          lastSolutions: {
            $push: {
              id: '$_id',
              points: { $sum: '$questions.mark' },
              submittedAt: '$submittedAt',
            },
          },
        },
    },
    {
        $lookup: {
          from: 'quizzes',
          localField: '_id',
          foreignField: '_id',
          as: 'quiz',
        },
    },
    {
      $unwind: '$quiz',
    },
    {
        $addFields : {
           points : { $sum: '$quiz.questions.point' }
          }
    }
  ]);
 
  return APIResponse.Ok(res, solutions);
});
