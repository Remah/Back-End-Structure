const $baseCtrl = require('../$baseCtrl');
const { APIResponse } = require('../../utils');
const models = require('../../models');

module.exports = $baseCtrl(async (req, res) => {
    const course = await models.course.findById(parseInt(req.params.cid))
    if(!course) return APIResponse.NotFound(res,'No Course With that id')

    const student = await models.student.findById(parseInt(req.params.sid))
    if(!student) return APIResponse.NotFound(res,'No Student with that id')

    if(course.teacher !== req.me.id && req.me.role !== 'admin')
        return APIResponse.Forbidden(res,'Dont allow to view these statistics')

    const progress = await models.courseProgress.findOne({course: course.id,user: student.id}).populate('lessons')

    return APIResponse.Ok(res,progress)

});
