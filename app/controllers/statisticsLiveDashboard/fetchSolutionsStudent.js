const $baseCtrl = require('../$baseCtrl');
const { APIResponse } = require('../../utils');
const models = require('../../models');

module.exports = $baseCtrl(async (req, res) => {
    console.log('here')
    const lid = parseInt(req.params.lid)
    if(isNaN(lid)) return APIResponse.BadRequest(res)
    const live = await models.live.findById(lid)
    if(!live) return APIResponse.NotFound(res,'No live With that id')

    if(live.teacher !== req.me.id && req.me.role !== 'admin')
        return APIResponse.Forbidden(res,'Dont allow to view these statistics')
    const sid = parseInt(req.params.sid)
    if(isNaN(sid)) return APIResponse.BadRequest(res)

    let quizes = await models.liveQuiz.find({live : lid})
    let quizesIds = quizes.map(quiz => quiz.id)
    const solutions = await models.solution.fetchAll(
        req.allowPagination,
        {quiz : { $in : quizesIds },student: sid },
        {
            ...req.queryOptions,
            populate: [{path: 'student', select:'username email photo phone'},'quiz']
        }
    )
    return APIResponse.Ok(res,solutions)

});
