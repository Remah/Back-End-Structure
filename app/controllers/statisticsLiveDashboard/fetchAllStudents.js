const $baseCtrl = require('../$baseCtrl');
const { APIResponse } = require('../../utils');
const models = require('../../models');

module.exports = $baseCtrl(async (req, res) => {
    const id = parseInt(req.params.id)
    if(isNaN(id)) return APIResponse.BadRequest(res)
    const live = await models.live.findById(id)
    if(!live) return APIResponse.NotFound(res,'No live With that id')

    if(live.teacher !== req.me.id && req.me.role !== 'admin')
        return APIResponse.Forbidden(res,'Dont allow to view these statistics')

    const progress = await models.liveProgress.fetchAll(
        req.allowPagination,
        {live: live.id},
        {
            ...req.queryOptions,
            populate: [{path: 'user', select:'username email photo phone'}]
        }
    )
    return APIResponse.Ok(res,progress)

});
