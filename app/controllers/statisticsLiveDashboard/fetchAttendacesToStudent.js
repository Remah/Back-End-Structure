const $baseCtrl = require('../$baseCtrl');
const { APIResponse } = require('../../utils');
const models = require('../../models');

module.exports = $baseCtrl(async (req, res) => {
    const lid = parseInt(req.params.lid)
    if(isNaN(lid)) return APIResponse.BadRequest(res)
    const live = await models.live.findById(lid)
    if(!live) return APIResponse.NotFound(res,'No live With that id')

    if(live.teacher !== req.me.id && req.me.role !== 'admin')
        return APIResponse.Forbidden(res,'Dont allow to view these statistics')

    const sid = parseInt(req.params.sid)
    if(isNaN(sid)) return APIResponse.BadRequest(res)

    let lectures = await models.lecture.fetchAll(
        req.allowPagination,
        {live: lid, users: sid},
        {
            ...req.queryOptions,
            select: 'title time'
        }
    )

    return APIResponse.Ok(res,lectures)

});
