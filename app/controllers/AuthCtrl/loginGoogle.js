const jwt = require("jsonwebtoken");
const $baseCtrl = require("../$baseCtrl");

module.exports = $baseCtrl(async (req, res) => {
  // console.log(req.authenticatedUser);
  const token = jwt.sign(
    { userId: req.authenticatedUser.id, userRole: req.authenticatedUser.role },
    process.env.JWT_SECRET
  );
  res.status(200).json({
    token,
    msg: "User Logged In With google !",
    user: req.authenticatedUser,
  });
});
