const $baseCtrl = require("../$baseCtrl");
const models = require("../../models");
const {APIResponse}  = require("../../utils");

module.exports = $baseCtrl(async (req, res) => {

    // code validation
    let usedCode = false
    let code
    if(req.body.code){
        code = await models.code.findOne({code:req.body.code})
        if(!code) return APIResponse.NotFound(res,'No code with that id')
        usedCode = true
    }

    // type of item with validation
    let subjectType = req.body.live ? 'live' : 'course'
    const id = parseInt(req.body[subjectType])
    if(isNaN(id)) return APIResponse.BadRequest(res,`Id of ${subjectType} is not valid`)
    const subjectObject = await models[subjectType].findById(id).populate('discount')
    if(!subjectObject) return APIResponse.NotFound(res,`No ${subjectType} with that id`)
    let subject = subjectObject.id

    // gift ? (create gift and order)
    let order
    if(req.body.email){

        let totalPrice = usedCode ? subjectObject.price - ( subjectObject.price * code.discount / 100) : subjectObject.price
        if(subjectObject.discount){
            if(subjectObject.discount.isValid){
                totalPrice = totalPrice - (totalPrice * subjectObject.discount.percentage / 100)
            }
        }
        order = await new models.order({
            user: req.me.id,
            subject,
            subjectType,
            totalPrice,
            isGift: true
        }).save()
        const gift = await new models.gift({
            order: order.id,
            sender: req.me.id,
            receiver: req.body.email
        }).save()
        console.log('gift',gift)

    } else {

        // not gift ? create order & progress
        let totalPrice = usedCode ? subjectObject.price - ( subjectObject.price * code.discount / 100) : subjectObject.price
        if(subjectObject.discount){
            if(subjectObject.discount.isValid){
                totalPrice = totalPrice - (totalPrice * subjectObject.discount.percentage / 100)
            }
        }
        if(subjectType === 'course'){
            let countLessons = await models.lesson.countDocuments({ course: subjectObject.id });
            let progress = await models.courseProgress.findOne({user: req.me.id , course: subjectObject.id})
            if(progress) return APIResponse.Forbidden(res,'You bought this course before')
            progress = await new models.courseProgress({
                user: req.me.id,
                teacher: subjectObject.teacher,
                course : subjectObject.id,
                numberOfCourseLessons : countLessons  
            }).save()
        } else {

            let progress = await models.liveProgress.findOne({user: req.me.id , live: subjectObject.id})
            if(progress) return APIResponse.Forbidden(res,'You bought this live before')
            progress = await new models.liveProgress({
                user: req.me.id,
                teacher: subjectObject.teacher,
                live : subjectObject.id
            }).save()
        }
        subjectObject.numberOfEnrolledStudents++
        await subjectObject.save()
        order = await new models.order({
            user: req.me.id,
            subject,
            subjectType,
            totalPrice
        }).save()
    

    }

    let response = await models.order.findById(order.id).populate([{ path : 'subject' , populate:['discount',{path:'teacher',select:'username photo'}]},'user'])

    return APIResponse.Ok(res,response)
    
});
