const $baseCtrl = require("../$baseCtrl");
const { APIResponse } = require("../../utils");
const models = require("../../models");

// [TODO] refactor this with html page
module.exports = $baseCtrl(async (req, res) => {
  let teachers = await models.teacher.find().sort("-rating").limit(10);
  return APIResponse.Ok(res, teachers);
});
