const $baseCtrl = require('../$baseCtrl');
const { APIResponse } = require('../../utils');
const models = require('../../models');

// [TODO] refactor this with html page
module.exports = $baseCtrl(async (req, res) => {
  let courses = await models.course
    .find({
      visibility: true,
    })
    .sort('-rating')
    .populate(["category","subCategory",'secondSubCategory',{path: "teacher" , select: 'username photo'},'discount'])
    .limit(10);
  // for (let i = 0; i < courses.length; i++) {
  //   if (courses[i].discount) {
  //     if (!courses[i].discount.isValid) {
  //       courses[i] = courses[i].toJSON({
  //         hide: 'discount',
  //       });
  //     }
  //   }
  // }
  return APIResponse.Ok(res, courses);
});
