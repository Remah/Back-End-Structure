const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');

module.exports = $baseCtrl(async (req, res) => {
    // aggregate skip which delete:true
    let stages = []
    if(req.query.category){
      const category_id = parseInt(req.query.category);
      if (isNaN(category_id) === true) {
        return APIResponse.BadRequest(res, 'category id must be a number');
      }
      stages.push({$match:{category:category_id}})
    }
    stages.push(
    { $group: { _id: '$student', points: { $sum: '$points' } } },
    { $sort: { points: -1 } },
    {
        $lookup: {
        from: 'users',
        localField: '_id',
        foreignField: '_id',
        as: 'user',
        },
    },
    {
        $unwind: '$user',
    },
    {
        $project: {
            _id: false,
            'user._id': true,
            'user.username': true,
            'user.photo': true,
            'user.email': true,
            'user.phone': true,
            'user.country':true,
            'points': true,
        },
    }
    )
  if(req.query.country) stages.push({ $match: {'user.country':req.query.country }})
  let points = await models.point.aggregate(stages)
  return APIResponse.Ok(res,points)
});

/*
const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');

module.exports = $baseCtrl(async (req, res) => {
  let points 
  if(req.query.country){
      points = await models.point.aggregate([
        { $group: { _id: '$student', points: { $sum: '$points' } } },
        { $sort: { points: -1 } },
        {
          $lookup: {
            from: 'users',
            localField: '_id',
            foreignField: '_id',
            as: 'user',
          },
        },
        {
          $unwind: '$user',
        },
        {
            $project: {
              _id: false,
              'user._id': true,
              'user.username': true,
              'user.photo': true,
              'user.email': true,
              'user.phone': true,
              'user.country':true,
              'points': true,
            },
        },
        { $match: {'user.country':req.query.country }}
      ])
  } else{
    points = await models.point.aggregate([
        { $group: { _id: '$student', points: { $sum: '$points' } } },
        { $sort: { points: -1 } },
        {
          $lookup: {
            from: 'users',
            localField: '_id',
            foreignField: '_id',
            as: 'user',
          },
        },
        {
          $unwind: '$user',
        },
        {
            $project: {
              _id: false,
              'user._id': true,
              'user.username': true,
              'user.photo': true,
              'user.email': true,
              'user.phone': true,
              'user.country':true,
              'points': true,
            },
        }
      ])
  }

  return APIResponse.Ok(res,points)
});

*/