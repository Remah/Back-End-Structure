const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');

module.exports = $baseCtrl(async (req, res) => {
  let response = {};
  let routePath = req.route.path.split('/');
  let subjectType = routePath[1] === 'teachers' ? 'teacher' : 'course';

  // Get Percentage Rates Card
  let AllRates = await models.rate.countDocuments({
    subjectType,
    subject: parseInt(req.params.id),
  });
  let percentageRates = await models.rate.aggregate([
    { $match: { subjectType, subject: parseInt(req.params.id) } },
    { $group: { _id: '$rating', numberOfRates: { $sum: 1 } } },
    {
      $project: {
        numberOfRates: 1,
        average: {
          $multiply: [{ $divide: ['$numberOfRates', AllRates] }, 100],
        },
      },
    },
    { $sort: { _id: -1 } },
  ]);
  // Rated Object : Course Or Teacher (to get rating from this object & teacher id 'if course')
  let ratedObject = await models[subjectType].findById(parseInt(req.params.id));
  // if Course ? So add teacher to response with total students in his courses and total courses
  if (subjectType === 'course') {
    let countCourses = await models.course.countDocuments({ teacher: ratedObject.teacher });
    let numberOfStudents = await models.courseProgress.countDocuments({
      teacher: ratedObject.teacher
    });
    let instructor = await models.teacher.findById(ratedObject.teacher);
    if (instructor) {
      instructor = instructor.toJSON();
      instructor.numberOfCourses = countCourses;
      instructor.numberOfStudents = numberOfStudents;
      response = { instructor };
    }
  }
  // get 5 feedbacks about this Rated Object
  let rates = await models.rate
    .find({ subjectType, subject: parseInt(req.params.id) })
    .sort('-createdAt')
    .limit(5)
    .populate({ path: 'user', select: 'username photo' });
  response = { ...response, rates, percentageRates, ratedObject };
  return APIResponse.Ok(res, response);
});
