const $baseCtrl = require("../$baseCtrl");
const models = require("../../models");
const { APIResponse } = require("../../utils");

module.exports = $baseCtrl(async (req, res) => {
  let routePath = req.route.path.split("/");
  let subjectType = routePath[1] === "teachers" ? "teacher" : "course";
  let rates = await models.rate.fetchAll(
    req.allowPagination,
    { subjectType, subject: parseInt(req.params.id) },
    {
      ...req.queryOptions,
      sort: "-createdAt",
      populate: { path: "user", select: "username photo" },
    }
  );
  return APIResponse.Ok(res, rates);
});
