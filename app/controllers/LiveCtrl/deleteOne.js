const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');

module.exports = $baseCtrl(async (req, res) => {
  const live = await models.live.findById(parseInt(req.params.id));
  if (!live) return APIResponse.NotFound(res, 'No live with that id');

  if (live.teacher !== req.me.id)
    return APIResponse.Forbidden(res, 'You dont allow to Delete this live course');

  await live.delete();

  return APIResponse.NoContent(res);
});
