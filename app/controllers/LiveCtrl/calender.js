const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');

module.exports = $baseCtrl(async (req, res) => {
  const live = await models.live.findById(parseInt(req.params.id)).populate('lecture');
  if (!live) return APIResponse.NotFound(res, 'No live with that id');

  // check if authorized
  if(req.me.role === 'student'){
    let progress = await models.liveProgress.findOne({user: req.me.id, live: live.id})
    if(!progress) return APIResponse.Forbidden(res,'You dont allow to view calender of this live')
  }

  let quizes = await models.liveQuiz.aggregate([
    {
      $match: {live : live.id , availability: true}
    },  
    {
      $addFields : {
         id : '$_id',
         durationInMS : {$multiply: ['$duration',60*1000]}
        }
    },
    {
      $addFields : {
         finishedAt: { $add : ['$startedAt','$durationInMS']} 
        }
    },
    {
      $match: { finishedAt : {$gt : new Date(new Date().toUTCString())}}
    },
    {
      $project: {
        _id: 0,
        durationInMS:0,
        __v:0
      }
    },
    {
      $addFields:{
        isLive : {
          $cond: [
            { $and: [ 
                { $gt: [ "$finishedAt", new Date(new Date().toUTCString()) ] },
                { $lte: [ "$startedAt", new Date(new Date().toUTCString()) ] }
            ]},    
            true, 
            false
        ]
        }
      }
    }
  ])  
  let response = {
      lecture: live.lecture,
      quizes
  }
  return APIResponse.Ok(res,response)
});
