const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');
const digitaloceanspaces = require('../../services/digitaloceanspaces');

module.exports = $baseCtrl([{ name: 'photo', maxCount: 1 }], digitaloceanspaces, async (req, res) => {
  const live = await models.live.findById(parseInt(req.params.id));
  if (!live) return APIResponse.NotFound(res, 'No live with that id');

  if (live.teacher !== req.me.id && req.me.role !== 'admin')
    return APIResponse.Forbidden(res, 'You dont allow to update this live course');

  if (req.files && req.files['photo']) {
    req.body.photo = req.files['photo'][0].location;
  }

  delete req.body.category;
  delete req.body.subCategory;
  delete req.body.secondSubCategory;
  delete req.body.teacher;

  if (req.body.discount !== undefined) {
    if(parseInt(req.body.discount) < 0)
        return APIResponse.BadRequest(res,'Discount is less than 0')
    if(parseInt(req.body.discount) !== 0){
      if (!req.body.startedAt || !req.body.finishedAt)
        return APIResponse.BadRequest(
          res,
          "Must provide startedAt and finishedAt to discount"
        );
    }
    // we have 2 cases : 1 - add discount
    if (parseInt(req.body.discount) !== 0) {
      // search for already exist discount applied before for same live
      let existDiscount = await models.discount.findOne({
        subject: live.id,
        subjectType: 'live'
      });
      if (existDiscount) {
        console.log("before edit", existDiscount);
        await existDiscount
          .set({
            percentage: req.body.discount,
            startedAt: req.body.startedAt,
            finishedAt: req.body.finishedAt,
          })
          .save();
        console.log("after edit", existDiscount);
        delete req.body.discount;
      } else {
        // NO discount apply before on this live
        let newDiscount = await new models.discount({
          subject: live.id,
          subjectType: 'live',
          percentage: req.body.discount,
          startedAt: req.body.startedAt,
          finishedAt: req.body.finishedAt,
        }).save();
        console.log("newDiscount", newDiscount);
        req.body.discount = newDiscount.id;
      }
      // here assign new values of date to product
      req.body.discountStartedAt = req.body.startedAt
      req.body.discountFinishedAt = req.body.finishedAt
    } else {
      // remove discount (which mean discount = zero)
      delete req.body.discount;
      live.discount = undefined;
      live.hasOfferNow = false
      live.discountStartedAt = undefined
      live.discountFinishedAt = undefined
      await live.save();
      let prevDiscount = await models.discount.findOne({
        subject: live.id,
        subjectType: 'live'
      });
      if (prevDiscount) await prevDiscount.delete();
    }
    delete req.body.finishedAt;
    delete req.body.startedAt;
  }

  if(req.me.role !== 'admin')
    delete req.body.price  

  await live.set(req.body).save();
  await models.live.populate(live,['discount'])
  return APIResponse.Ok(res, live);
});
