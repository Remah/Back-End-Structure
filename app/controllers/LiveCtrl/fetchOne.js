const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');

module.exports = $baseCtrl(async (req, res) => {
  let live = await models.live
    .findById(parseInt(req.params.id))
    .populate('discount')
    .select('-lecture');
    if (!live) return APIResponse.NotFound(res, 'No live with that id');
    
    let hideDiscount = 0;
    if (live.discount) {
      if (live.discount.isExpired) {
        live.discount = undefined;
        live.hasOfferNow = false;
        let prevDiscount = await models.discount.findOne({ subject : live.id, subjectType: 'live' });
        if (prevDiscount) await prevDiscount.delete();
      } else if (live.discount.isValid) {
        live.hasOfferNow = true;
      } else {
        // offer not start
        hideDiscount = 1;
        live.hasOfferNow = false;
      }
      await live.save();
    }
    if (hideDiscount) {
      live = live.toJSON({ hide: 'discount' });
    }
  return APIResponse.Ok(res, live);
});
