const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');

module.exports = $baseCtrl(async (req, res) => {
  const secondSubCategory = await models.secondSubCategory.findById(req.params.id);
  if (!secondSubCategory) return APIResponse.NotFound(res, "No secondSubCategory with that id");
  

  //  // filter expire discounts and remove them
  //  let allDiscounts = await models.discount.find({subjectType:'live'});
  // //  console.log(allDiscounts)
  //  for (let i = 0; i < allDiscounts.length; i++) {
  //    let live = await models.live.findById(allDiscounts[i].subject);
  //    if (!live) continue;
  //    if (allDiscounts[i].isExpired) {
  //      live.discount = undefined;
  //      live.hasOfferNow = false;
  //      await allDiscounts[i].delete();
  //    } else if (allDiscounts[i].isValid) {
  //      live.hasOfferNow = true;
  //    } else {
  //      // offer not start
  //      live.hasOfferNow = false;
  //    }
  //    await live.save();
  //  }

  const liveCousres = await models.live.fetchAll(
    req.allowPagination,
    {
      secondSubCategory: secondSubCategory.id,
      visibility: true,
      liveFinishedAt: { $gt: new Date(new Date().toUTCString()) },
      ...req.queryFilter,
    },
    {
      ...req.queryOptions,
      populate: [{path: "teacher" , select: 'username photo'},"category","subCategory",'secondSubCategory','discount'],
      select: '-lecture',
    },
  );

  
  return APIResponse.Ok(res, liveCousres);
});
