const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');
const digitaloceanspaces = require('../../services/digitaloceanspaces');

module.exports = $baseCtrl([{ name: 'photo', maxCount: 1 }], digitaloceanspaces, async (req, res) => {
  const secondSubCategory = await models.secondSubCategory.findById(req.params.id);
  if (!secondSubCategory) return APIResponse.NotFound(res, "No secondSubCategory with that id");

  delete req.body.discount
  req.body.category = secondSubCategory.category;
  req.body.subCategory = secondSubCategory.subCategory
  req.body.secondSubCategory = secondSubCategory.id
  req.body.teacher = req.me.id;

  if (req.files && req.files['photo']) {
    req.body.photo = req.files['photo'][0].location;
  }
  delete req.body.price
  const live = await new models.live(req.body).save();
  return APIResponse.Created(res, live);
});
