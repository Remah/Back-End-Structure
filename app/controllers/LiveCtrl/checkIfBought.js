const $baseCtrl = require("../$baseCtrl");
const models = require("../../models");
const { APIResponse } = require("../../utils");

module.exports = $baseCtrl(async (req, res) => {
  const id = parseInt(req.params.id);
  if (isNaN(id)) return APIResponse.NotFound(res);

  const live = await models.live.findById(id);
  if (!live) return APIResponse.NotFound(res, "No live with that id");
  let hasBought = false 
  if(req.me){
      let progress = await models.liveProgress.findOne({user: req.me.id , live: id})
      if(progress) hasBought = true
  }
  return APIResponse.Ok(res,{hasBought})
});
