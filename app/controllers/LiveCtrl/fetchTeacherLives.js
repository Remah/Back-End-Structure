const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');

module.exports = $baseCtrl(async (req, res) => {
   

  const liveCousres = await models.live.fetchAll(
    req.allowPagination,
    {
        teacher:req.me.id,
      ...req.queryFilter
    },
    {
      ...req.queryOptions,
      populate: [,"category","subCategory",'secondSubCategory','discount']
    },
  );

  
  return APIResponse.Ok(res, liveCousres);
});
