const $baseCtrl = require("../$baseCtrl");
const models = require("../../models");
const { APIResponse } = require("../../utils");

module.exports = $baseCtrl(async (req, res) => {
  const id = parseInt(req.params.id);
  if (isNaN(id)) return APIResponse.NotFound(res);

  const secondSubCategory = await models.secondSubCategory.findById(id);
  if (!secondSubCategory) return APIResponse.NotFound(res,'No secondSubCategory with that id');
  return APIResponse.Ok(res, secondSubCategory);
});
