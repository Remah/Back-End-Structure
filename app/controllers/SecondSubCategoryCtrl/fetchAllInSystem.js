const $baseCtrl = require("../$baseCtrl");
const models = require("../../models");
const { APIResponse } = require("../../utils");

module.exports = $baseCtrl(async (req, res) => {
  const ssc = await models.secondSubCategory.find().select('nameAr nameEn')
  return APIResponse.Ok(res,ssc)
});
