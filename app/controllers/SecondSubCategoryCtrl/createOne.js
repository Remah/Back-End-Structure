const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');
const digitaloceanspaces = require('../../services/digitaloceanspaces');

module.exports = $baseCtrl(
  [{ name: 'photo', maxCount: 1 }],
  digitaloceanspaces,
  async (req, res) => {

    const subCategory = await models.subCategory.findById(parseInt(req.params.id))
    if(!subCategory) return APIResponse.NotFound(res,'No subCategory with that id')

    if (req.files && req.files['photo']) {
      req.body.photo = req.files['photo'][0].location;
    }

    req.body.category = subCategory.category
    req.body.subCategory = subCategory.id

    const secondSubCategory = await new models.secondSubCategory(req.body).save();
    return APIResponse.Created(res, secondSubCategory);

  }
);
