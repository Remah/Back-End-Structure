const $baseCtrl = require("../$baseCtrl");
const models = require("../../models");
const { APIResponse } = require("../../utils");
const digitaloceanspaces = require("../../services/digitaloceanspaces");

module.exports = $baseCtrl(
  [{ name: "photo", maxCount: 1 }],
  digitaloceanspaces,
  async (req, res) => {
    const id = parseInt(req.params.id);
    if (isNaN(id)) return APIResponse.NotFound(res);

    const secondSubCategory = await models.secondSubCategory.findById(id)
    if(!secondSubCategory) return APIResponse.NotFound(res,'No secondSubCategory with that id')

    if (req.files && req.files["photo"]) {
      req.body.photo = req.files["photo"][0].location;
    }

    delete req.body.category
    delete req.body.subCategory

    await secondSubCategory.set(req.body).save();
    return APIResponse.Ok(res, secondSubCategory);
  }
);
