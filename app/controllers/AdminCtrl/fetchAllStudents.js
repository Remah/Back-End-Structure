const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');
const { escapeRegExp } = require('lodash');

module.exports = $baseCtrl(async (req, res) => {
  let query = {};
  if (req.query.name) query.username = new RegExp(escapeRegExp(req.query.name), 'i');
  if (req.query.email) query.email = new RegExp(escapeRegExp(req.query.email), 'i');
  if (req.query.phone) query.phone = new RegExp(escapeRegExp(req.query.phone), 'i');
  let students = await models.student.fetchAll(
    req.allowPagination,
    query,
    {
    ...req.queryOptions
    }
  );
  return APIResponse.Ok(res, students);
});
