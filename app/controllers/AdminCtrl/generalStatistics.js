const $baseCtrl = require("../$baseCtrl");
const { APIResponse } = require("../../utils");
const models = require("../../models");

module.exports = $baseCtrl(async (req, res) => {
    let students = await models.student.countDocuments()    
    let teachers = await models.teacher.countDocuments()    
    let users = await models._user.estimatedDocumentCount()    
    let courses = await models.course.estimatedDocumentCount()
    let lives = await models.live.estimatedDocumentCount()
    let inquiries = await models.inquiry.estimatedDocumentCount()
    let coursesEnrollments = await models.courseProgress.countDocuments()
    let liveEnrollments = await models.liveProgress.countDocuments()
    let allEnrollments = await models._progress.estimatedDocumentCount()
    let gifts = await models.gift.estimatedDocumentCount()
    let quizes = await models._quiz.estimatedDocumentCount()
    let lectures = await models.lecture.estimatedDocumentCount()
    let lessons = await models.lesson.estimatedDocumentCount()
    let categories = await models.category.estimatedDocumentCount()
    let subCategories = await models.subCategory.estimatedDocumentCount()
    let secondSubCategories = await models.secondSubCategory.estimatedDocumentCount()
    let response = {
        students,
        teachers,
        users,
        courses,
        lives,
        inquiries,
        coursesEnrollments,
        liveEnrollments,
        allEnrollments,
        gifts,
        quizes,
        lectures,
        lessons,
        categories,
        subCategories,
        secondSubCategories
    }
    return APIResponse.Ok(res,response)
});
