const $baseCtrl = require("../$baseCtrl");
const { APIResponse } = require("../../utils");
const models = require("../../models");

module.exports = $baseCtrl(
    async (req, res) => {
    
    let requests = await models.request.fetchAll(
        req.allowPagination,
        req.queryFilter,
        {...req.queryOptions,sort:'-createdAt'}
    )
    return APIResponse.Ok(res,requests)
});
