const $baseCtrl = require("../$baseCtrl");
const models = require("../../models");
const { APIResponse } = require("../../utils");
const bcrypt = require("bcryptjs");
const digitaloceanspaces = require("../../services/digitaloceanspaces");



module.exports = $baseCtrl(
  [{ name: "photo", maxCount: 1 },{ name: "description", maxCount: 1 }],
  digitaloceanspaces,
  async (req, res) => {
   
    // Check if values not entered
    if (
      req.body.username === undefined ||
      req.body.password === undefined ||
      req.body.email === undefined ||
      req.body.gender === undefined ||
      req.body.country === undefined
    ) {
      return APIResponse.BadRequest(res, "You have to fill all options .");
    }

    // Check if E-mail Already Exist
    let user = await models._user.findOne({ email: req.body.email });
    if (user) {
      return APIResponse.BadRequest(res, " Email Already in use .");
    }

    // Check if phone Already Exist
    if (req.body.phone) {
      let existPhone = await models._user.findOne({ phone: req.body.phone });
      if (existPhone) {
        return APIResponse.BadRequest(res, " phone Already in use .");
      }
    }

    // Encrypt Password
    var salt = bcrypt.genSaltSync(10);
    var hash = bcrypt.hashSync(req.body.password, salt);
    req.body.password = hash;

    // Upload photo if enter by user
    if (req.files && req.files["photo"]) {
      req.body.photo = req.files["photo"][0].location;
    }

    if (req.files && req.files["description"]) {
      req.body.description = req.files["description"][0].location;
    }

    req.body.role = 'teacher'
    req.body.enabled = true
    
    // save user to db
    const newUser = await new models.teacher(req.body).save();

    const response = {
      user: newUser,
    };

    return APIResponse.Created(res, response);
  }
);
