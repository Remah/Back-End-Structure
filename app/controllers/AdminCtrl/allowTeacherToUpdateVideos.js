const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');
const { escapeRegExp } = require('lodash');

module.exports = $baseCtrl(async (req, res) => {
  const id = parseInt(req.params.id)
  if(isNaN(id)) return APIResponse.BadRequest(res)
  const teacher = await models.teacher.findById(id)
  if(!teacher) return APIResponse.NotFound(res,'No Teacher with that id')

  teacher.isAllowedUpdateVideos = req.body.isAllowedUpdateVideos

  await teacher.save()

  return APIResponse.Ok(res,teacher)
});
