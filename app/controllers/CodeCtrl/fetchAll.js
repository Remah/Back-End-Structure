const $baseCtrl = require("../$baseCtrl");
const models = require("../../models");
const {APIResponse}  = require("../../utils");

module.exports = $baseCtrl(async (req, res) => {
  const codes = await models.code.find()
  return APIResponse.Ok(res,codes)
});
