const $baseCtrl = require("../$baseCtrl");
const models = require("../../models");
const {APIResponse}  = require("../../utils");

module.exports = $baseCtrl(async (req, res) => {
    const code = await models.code.findOne({code:req.body.code})
    if(!code) return APIResponse.NotFound(res,'No code with that id')

    return APIResponse.Ok(res,code)
});
