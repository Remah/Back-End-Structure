const $baseCtrl = require("../$baseCtrl");
const models = require("../../models");
const {APIResponse}  = require("../../utils");

module.exports = $baseCtrl(async (req, res) => {

  const prevCode = await models.code.findOne({code:req.body.code})
  if(prevCode) return APIResponse.Forbidden(res,'You have add this code before')

  const code = await new models.code(req.body).save()
  return APIResponse.Created(res,code)
});
