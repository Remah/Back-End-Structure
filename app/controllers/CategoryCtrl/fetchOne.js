const $baseCtrl = require("../$baseCtrl");
const models = require("../../models");
const { APIResponse } = require("../../utils");

module.exports = $baseCtrl(async (req, res) => {
  const id = parseInt(req.params.id);
  if (isNaN(id)) return APIResponse.NotFound(res);

  let category = await models.category.findById(id);
  if (!category) return APIResponse.NotFound(res);
  return APIResponse.Ok(res, category);
});
