const $baseCtrl = require("../$baseCtrl");
const models = require("../../models");
const { APIResponse } = require("../../utils");
const digitaloceanspaces = require("../../services/digitaloceanspaces");

module.exports = $baseCtrl(
  [{ name: "photo", maxCount: 1 }],
  digitaloceanspaces,
  async (req, res) => {
    const id = parseInt(req.params.id);
    if (isNaN(id)) return APIResponse.NotFound(res);

    const category = await models.category.findById(id);
    if (!category) return APIResponse.NotFound(res);

    if (req.files && req.files["photo"]) {
      req.body.photo = req.files["photo"][0].location;
    }

    await category.set(req.body).save();
    return APIResponse.Ok(res, category);
  }
);
