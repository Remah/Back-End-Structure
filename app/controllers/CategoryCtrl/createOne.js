const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');
const digitaloceanspaces = require('../../services/digitaloceanspaces');

module.exports = $baseCtrl(
  [{ name: 'photo', maxCount: 1 }],
  digitaloceanspaces,
  async (req, res) => {
    if (req.files && req.files['photo']) {
      req.body.photo = req.files['photo'][0].location;
    }
    const category = await new models.category(req.body).save();
    return APIResponse.Created(res, category);

  }
);
