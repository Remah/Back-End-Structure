const $baseCtrl = require("../$baseCtrl");
const models = require("../../models");
const { APIResponse } = require("../../utils");
const digitaloceanspaces = require("../../services/digitaloceanspaces");

module.exports = $baseCtrl(
  [{ name: "image", maxCount: 1 }],
  digitaloceanspaces,
  async (req, res) => {
    const comment = await models.comment.findById(req.params.id);
    if (!comment) return APIResponse.NotFound(res, "No comment with that id");
    if (
      comment.user !== req.authenticatedUser.id &&
      req.authenticatedUser.role === "student"
    )
      return APIResponse.Forbidden(res, "You cant update this comment");

    delete req.body.replies;
    delete req.body.user;

    if (req.files && req.files["image"]) {
      req.body.image = req.files["image"][0].location;
    } else delete req.body.image;

    await comment.set(req.body).save();

    let commentResponce = await models.comment.populate(comment, [
      { path: "user", select: "_id username  photo" },
      { path: "replies.user", select: "_id username  photo" },
    ]);

    return APIResponse.Ok(res, commentResponce);
  }
);
