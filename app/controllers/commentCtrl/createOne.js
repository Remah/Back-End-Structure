const $baseCtrl = require("../$baseCtrl");
const models = require("../../models");
const { APIResponse } = require("../../utils");
const digitaloceanspaces = require("../../services/digitaloceanspaces");
// const nodemailer = require("nodemailer");

// config gmail
// const transproter = nodemailer.createTransport({
//   service: 'Gmail',
//   auth: {
//     user: process.env.gmail,
//     pass: process.env.passGmail,
//   },
// });

module.exports = $baseCtrl(
  [{ name: "image", maxCount: 1 }],
  digitaloceanspaces,
  async (req, res) => {
    const user = req.authenticatedUser;

    let routePath = req.route.path.split("/");
    let type = routePath[1] === "lessons" ? "lesson" : "course";

    const post = await models[type].findById(req.params.id);
    if (!post) return APIResponse.NotFound(res, `No ${type} with that id`);

    req.body.user = user.id;
    req.body.post = post.id;
    req.body.type = type;
    req.body.replies = [];

    if (req.files && req.files["image"]) {
      req.body.image = req.files["image"][0].location;
    } else delete req.body.image;

    const newComment = await new models.comment(req.body).save();
    let comment = await models.comment.populate(newComment, [
      { path: "user", select: "_id username  photo" },
      { path: "post" },
    ]);

    // send notification if in post
    // if (type === 'post') {
    //   const initiator = req.authenticatedUser;
    //   const receiver = await models._user.findById(post.user);
    //   if (initiator.id !== receiver.id) {
    //     let genderSpec =
    //       initiator.gender === 'female' ? 'قامت' : 'قام';
    //     let title = '📝 تم إضافة تعليق على منشور لك';
    //     let body = `${genderSpec} ${initiator.username} بالتعليق على منشور لك`;
    //     const notification = await models
    //       .notification({
    //         title: title,
    //         body: body,
    //         initiator: initiator.id,
    //         receiver: receiver.id,
    //         subjectType: 'post',
    //         subject: post.id,
    //       })
    //       .save();
    //     await receiver.sendNotification(
    //       notification.toFirebaseNotification(),
    //     );
    //     console.log('initator', initiator);
    //     console.log('receiver', receiver);
    //     console.log(notification);

    //     // send mail
    //     // transproter.sendMail({
    //     //     to: receiver.email,
    //     //     from: process.env.gmail,
    //     //     subject: title,
    //     //     text: body,
    //     // }, (err, info) => {
    //     //     console.log('info', info.messageId);
    //     //     console.log('error', err);
    //     // })
    //   }
    // }
    return APIResponse.Created(res, comment);
  }
);
