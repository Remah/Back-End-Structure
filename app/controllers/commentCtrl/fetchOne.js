const $baseCtrl = require("../$baseCtrl");
const models = require("../../models");
const { APIResponse } = require("../../utils");

module.exports = $baseCtrl(async (req, res) => {
  const comment = await models.comment
    .findById(req.params.id)
    .populate([
      { path: "user", select: "_id username  photo" },
      { path: "replies.user", select: "_id username  photo" },
      { path: "post" },
    ]);
  if (!comment) return APIResponse.NotFound(res, "No comment with that id");
  return APIResponse.Ok(res, comment);
});
