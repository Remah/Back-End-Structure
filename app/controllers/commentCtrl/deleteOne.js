const $baseCtrl = require("../$baseCtrl");
const models = require("../../models");
const { APIResponse } = require("../../utils");

module.exports = $baseCtrl(async (req, res) => {
  const comment = await models.comment.findById(req.params.id);
  if (!comment) return APIResponse.NotFound(res);
  if (
    comment.user !== req.authenticatedUser.id &&
    req.authenticatedUser.role === "student"
  )
    return APIResponse.Forbidden(res, "You cant delete this comment");

  await comment.delete();

  return APIResponse.NoContent(res);
});
