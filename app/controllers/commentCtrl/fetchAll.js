const $baseCtrl = require("../$baseCtrl");
const models = require("../../models");
const { APIResponse } = require("../../utils");

module.exports = $baseCtrl(async (req, res) => {
  let routePath = req.route.path.split("/");
  let type = routePath[1] === "lessons" ? "lesson" : "course";

  const post = await models[type].findById(req.params.id);
  if (!post) return APIResponse.NotFound(res, "No post with that id");

  const comments = await models.comment.fetchAll(
    req.allowPagination,
    { ...req.queryFilter, post: post.id, type },
    {
      ...req.queryOptions,
      populate: [
        { path: "user", select: "_id username  photo" },
        { path: "replies.user", select: "_id username  photo" },
        { path: "post" },
      ],
      sort: { createdAt: -1 },
    }
  );

  return APIResponse.Ok(res, comments);
});
