const $baseCtrl = require("../$baseCtrl");
const models = require("../../models");
const { APIResponse } = require("../../utils");

module.exports = $baseCtrl(async (req, res) => {
  let gifts = await models.gift.find({receiver: req.me.email})
    .populate([
       {path: 'order',populate: { path : 'subject' , populate:['discount',{path: 'teacher',select: 'username photo'}]}},'sender'
    ])
  return APIResponse.Ok(res,gifts)
});
