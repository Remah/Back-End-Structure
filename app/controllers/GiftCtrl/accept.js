const $baseCtrl = require("../$baseCtrl");
const models = require("../../models");
const { APIResponse } = require("../../utils");

module.exports = $baseCtrl(async (req, res) => {
  const id = parseInt(req.params.id)
  if(isNaN(id)) return APIResponse.NotFound(res)
  let gift = await models.gift.findById(id).populate([{path: 'order',populate:['subject']},'sender'])
  if(!gift) return APIResponse.NotFound(res,'NO gift with that id')

  if(gift.receiver !== req.me.email)
    return APIResponse.Forbidden(res,'You dont allow to accept this gift')

  let progressType = gift.order.subjectType === 'course' ?  'courseProgress' : 'liveProgress'
  const filter = {
      ...(progressType === 'courseProgress' && { course: gift.order.subject.id }),
      ...(progressType === 'liveProgress' && { live: gift.order.subject.id }),
      user: req.me.id
  }
  let progress = await models[progressType].findOne(filter)
  if(progress) return APIResponse.Forbidden(res,'You already have this course in your profile')  

  if(progressType === 'courseProgress'){
    let countLessons = await models.lesson.countDocuments({ course: gift.order.subject.id });
    progress = await new models.courseProgress({
        user: req.me.id,
        teacher: gift.order.subject.teacher,
        course : gift.order.subject.id,
        numberOfCourseLessons : countLessons  
    }).save()
    let courseIncrement = await models.course.findById(gift.order.subject.id)
    courseIncrement.numberOfEnrolledStudents++
    await courseIncrement.save()
    } else {
        progress = await new models.liveProgress({
            user: req.me.id,
            teacher: gift.order.subject.teacher,
            live : gift.order.subject.id
        }).save()
        let liveIncrement = await models.live.findById(gift.order.subject.id)
        liveIncrement.numberOfEnrolledStudents++
        await liveIncrement.save()
    }

    await gift.set({used: true}).save()

  return APIResponse.Ok(res,progress)
});
