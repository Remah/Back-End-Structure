const $baseCtrl = require("../$baseCtrl");
const models = require("../../models");
const { APIResponse } = require("../../utils");

module.exports = $baseCtrl(async (req, res) => {
  const id = parseInt(req.params.id)
  if(isNaN(id)) return APIResponse.BadRequest(res)
  let gift = await models.gift.findById(id)
    .populate([
       {path: 'order',populate: { path : 'subject' , populate:['discount',{path: 'teacher',select: 'username photo'}]}},'sender'
    ])
  if(req.me.email !== gift.receiver)
    return APIResponse.Forbidden(res,'Dont allow to view this gift ')
  return APIResponse.Ok(res,gift)
});
