const $baseCtrl = require("../$baseCtrl");
const models = require("../../models");
const { APIResponse } = require("../../utils");

module.exports = $baseCtrl(async (req, res) => {
    const course = parseInt(req.body.course)
    const courseExist = await models.course.findById(course)
    if(!courseExist) return APIResponse.NotFound(res,'No course with that id')
    // Not authorized user
    if(!req.authenticatedUser){
        // two cases : 1. has prev doc , 2. generate new doc
        let document = await models.recommend.findOne({deviceToken: req.body.deviceToken})
        if(document){
           document.lastCategorySurvey = courseExist.secondSubCategory
           if(document.courses.indexOf(course) === -1)
           {
               document.courses.push(course)
           }
           await document.save()
        } else {
            document = await new models.recommend({deviceToken: req.body.deviceToken,courses:[course],lastCategorySurvey:courseExist.secondSubCategory}).save()
        }
        return APIResponse.Ok(res,document)
    } else {
        // Authoirzed user
        req.authenticatedUser.lastCategorySurvey = courseExist.secondSubCategory
        if(req.authenticatedUser.recommended.indexOf(course) === -1){
            req.authenticatedUser.recommended.push(course)
        }
        await req.authenticatedUser.save()
        return APIResponse.Ok(res,req.authenticatedUser.recommended)
    }

});
