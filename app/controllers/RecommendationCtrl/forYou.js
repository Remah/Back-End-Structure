const $baseCtrl = require("../$baseCtrl");
const models = require("../../models");
const { APIResponse } = require("../../utils");

module.exports = $baseCtrl(async (req, res) => {
    if(!req.query.type)
        return APIResponse.BadRequest(res,'Must provide type of recommendation')
    let type = req.query.type === 'survey' ? 'lastCategorySurvey' : 
        req.query.type === 'favourite' ? 'lastCategoryFavourite' : //  only auth user has favourite
            'lastCategorySearch'
    // Not authorized user
    let recommendedCourses = {}
    if(!req.authenticatedUser){

        if(!req.body.deviceToken) return APIResponse.BadRequest(res,'Must provide deviceToken')

        let document = await models.recommend.findOne({deviceToken: req.body.deviceToken})
        
        if(!document){ // No Prev history in general
            recommendedCourses = await models.course.fetchAll(
                req.allowPagination,
                {visibility: true},
                {
                    ...req.queryOptions,
                    populate: [{path: "teacher" , select: 'username photo'}, "secondSubCategory",'category','subCategory','discount']
                }
            )
            recommendedCourses.type = req.query.type
            recommendedCourses.nameAr = 'Goud Recommendation'
            recommendedCourses.nameEn = 'مقترحات جود'
            // return APIResponse.Ok(res,recommendedCourses)
            return APIResponse.Ok(res,{docs:[]})
        }
        
        if(document[type] === undefined){ // No Prev history in this category
            recommendedCourses = await models.course.fetchAll(
                req.allowPagination,
                {visibility: true},
                {
                    ...req.queryOptions,
                    populate: [{path: "teacher" , select: 'username photo'}, "secondSubCategory",'category','subCategory','discount']
                }
            )
            recommendedCourses.type = req.query.type
            recommendedCourses.nameAr = 'Goud Recommendation'
            recommendedCourses.nameEn = 'مقترحات جود'
            return APIResponse.Ok(res,{docs:[]})
            // return APIResponse.Ok(res,recommendedCourses)
        }

        // if user has doc in recommend model & has prev history in this category
         recommendedCourses = await models.course.fetchAll(
            req.allowPagination,
            {secondSubCategory:document[type],_id: {$nin : document.courses}, visibility: true},
            {
                ...req.queryOptions,
                populate: [{path: "teacher" , select: 'username photo'}, "secondSubCategory",'category','subCategory','discount'],
                sort: '-createdAt'
            }
        )
        recommendedCourses.type = req.query.type
        recommendedCourses.nameAr = recommendedCourses.docs[0] ? recommendedCourses.docs[0].secondSubCategory.nameAr : undefined
        recommendedCourses.nameEn = recommendedCourses.docs[0] ? recommendedCourses.docs[0].secondSubCategory.nameEn : undefined
    } else {
        // Authorized User
        if(req.me[type] === undefined){ // if no prev history in this category
            // return random courses
            recommendedCourses = await models.course.fetchAll(
                req.allowPagination,
                {visibility: true},
                {
                    ...req.queryOptions,
                    populate: [{path: "teacher" , select: 'username photo'}, "secondSubCategory",'category','subCategory','discount']
                }
            )
            recommendedCourses.type = req.query.type
            recommendedCourses.nameAr = 'Goud Recommendation'
            recommendedCourses.nameEn = 'مقترحات جود'
            // return APIResponse.Ok(res,recommendedCourses)
            return APIResponse.Ok(res,{docs:[]})
        }
        recommendedCourses = await models.course.fetchAll(
        req.allowPagination,
        {secondSubCategory:req.me[type],_id: {$nin : req.me.recommended}, visibility: true},
        {
            ...req.queryOptions,
            populate: [{path: "teacher" , select: 'username photo'}, "secondSubCategory",'category','subCategory','discount'],
            sort: '-createdAt'
        }
        )
        recommendedCourses.type = req.query.type
        recommendedCourses.nameAr = recommendedCourses.docs[0] ? recommendedCourses.docs[0].secondSubCategory.nameAr : undefined
        recommendedCourses.nameEn = recommendedCourses.docs[0] ? recommendedCourses.docs[0].secondSubCategory.nameEn : undefined
    }
    return APIResponse.Ok(res,recommendedCourses)
});



/*
const $baseCtrl = require("../$baseCtrl");
const models = require("../../models");
const { APIResponse } = require("../../utils");

module.exports = $baseCtrl(async (req, res) => {
    // if(!req.query.type)
    //     return APIResponse.BadRequest(res,'Must provide type of recommendation')
    let secondSubCategory = 
    // Not authorized user
    let recommendedCourses = {}
    if(!req.authenticatedUser){
        if(!req.body.deviceToken) return APIResponse.BadRequest(res,'Must provide deviceToken')
        // if user does not has doc in recommend model
        let document = await models.recommend.findOne({deviceToken: req.body.deviceToken})
        // return random courses
        if(!document){
            recommendedCourses = await models.course.fetchAll(
                req.allowPagination,
                {visibility: true},
                {
                    ...req.queryOptions,
                    populate: [{path: "teacher" , select: 'username photo'}, "secondSubCategory",'category','subCategory','discount']
                }
            )
            return APIResponse.Ok(res,recommendedCourses)
        }
        // if user has doc in recommend model
         recommendedCourses = await models.course.fetchAll(
            req.allowPagination,
            {secondSubCategory:document.lastCategory,_id: {$nin : document.courses}, visibility: true},
            {
                ...req.queryOptions,
                populate: [{path: "teacher" , select: 'username photo'}, "secondSubCategory",'category','subCategory','discount'],
                sort: '-createdAt'
            }
        )
    } else {
        // Authorized User
        if(req.authenticatedUser.recommended.length === 0){
            // return random courses
            recommendedCourses = await models.course.fetchAll(
                req.allowPagination,
                {visibility: true},
                {
                    ...req.queryOptions,
                    populate: [{path: "teacher" , select: 'username photo'}, "secondSubCategory",'category','subCategory','discount']
                }
            )
            return APIResponse.Ok(res,recommendedCourses)
        }
        recommendedCourses = await models.course.fetchAll(
        req.allowPagination,
        {secondSubCategory:req.me.lastCategory,_id: {$nin : req.me.recommended}, visibility: true},
        {
            ...req.queryOptions,
            populate: [{path: "teacher" , select: 'username photo'}, "secondSubCategory",'category','subCategory','discount'],
            sort: '-createdAt'
        }
        )
    }
    return APIResponse.Ok(res,recommendedCourses)
});
*/

