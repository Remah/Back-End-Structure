const $baseCtrl = require("../$baseCtrl");
const { APIResponse } = require("../../utils");
const models = require("../../models");
const digitaloceanspaces = require("../../services/digitaloceanspaces");

module.exports = $baseCtrl(
    [{ name: "cv", maxCount: 1 },{ name: 'photo', maxCount: 1}],
    digitaloceanspaces,
    async (req, res) => {
    if (req.files && req.files["cv"]) {
        req.body.cv = req.files["cv"][0].location;
    }   
    if (req.files && req.files["photo"]) {
        req.body.photo = req.files["photo"][0].location;
    }     
    
    let request = await new models.request(req.body).save()
    return APIResponse.Ok(res,request)
});
