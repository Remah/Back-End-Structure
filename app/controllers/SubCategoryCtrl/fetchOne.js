const $baseCtrl = require("../$baseCtrl");
const models = require("../../models");
const { APIResponse } = require("../../utils");

module.exports = $baseCtrl(async (req, res) => {
  const id = parseInt(req.params.id);
  if (isNaN(id)) return APIResponse.NotFound(res);

  let subCategory = await models.subCategory.findById(id);
  if (!subCategory) return APIResponse.NotFound(res,'No SubCategory with that id');
  return APIResponse.Ok(res, subCategory);
});
