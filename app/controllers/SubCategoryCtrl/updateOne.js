const $baseCtrl = require("../$baseCtrl");
const models = require("../../models");
const { APIResponse } = require("../../utils");
const digitaloceanspaces = require("../../services/digitaloceanspaces");

module.exports = $baseCtrl(
  [{ name: "photo", maxCount: 1 }],
  digitaloceanspaces,
  async (req, res) => {
    const id = parseInt(req.params.id);
    if (isNaN(id)) return APIResponse.NotFound(res);

    const subCategory = await models.subCategory.findById(id)
    if(!subCategory) return APIResponse.NotFound(res,'No subCategory with that id')

    if (req.files && req.files["photo"]) {
      req.body.photo = req.files["photo"][0].location;
    }

    delete req.body.category

    await subCategory.set(req.body).save();
    return APIResponse.Ok(res, subCategory);
  }
);
