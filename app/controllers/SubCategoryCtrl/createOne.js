const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');
const digitaloceanspaces = require('../../services/digitaloceanspaces');

module.exports = $baseCtrl(
  [{ name: 'photo', maxCount: 1 }],
  digitaloceanspaces,
  async (req, res) => {

    const category = await models.category.findById(parseInt(req.params.id))
    if(!category) return APIResponse.NotFound(res,'No category with that id')

    if (req.files && req.files['photo']) {
      req.body.photo = req.files['photo'][0].location;
    }

    req.body.category = category.id

    const subCategory = await new models.subCategory(req.body).save();

    await category.set({visibility: true}).save()

    return APIResponse.Created(res, subCategory);

  }
);
