const $baseCtrl = require("../$baseCtrl");
const models = require("../../models");
const { APIResponse } = require("../../utils");
const digitaloceanspaces = require("../../services/digitaloceanspaces");

module.exports = $baseCtrl(
  [{ name: "photo", maxCount: 1 },{ name: 'video', maxCount: 1}],
  digitaloceanspaces,
  async (req, res) => {
    const id = parseInt(req.params.id);
    if (isNaN(id)) return APIResponse.NotFound(res);

    const course = await models.course.findById(id);
    if (!course) return APIResponse.NotFound(res, "No course with that id");

    if(course.teacher !== req.me.id && req.me.role !== 'admin')
      return APIResponse.Forbidden(res,'Dont allow to update this course')

    if(req.me.role !== 'admin')
      delete req.body.status
      
    if (req.files && req.files["photo"]) {
      req.body.photo = req.files["photo"][0].location;
    }
    if (req.files && req.files["video"]) {
      req.body.video = req.files["video"][0].location;
    }
    delete req.body.category;
    delete req.body.subCategory;
    delete req.body.secondSubCategory;
    delete req.body.teacher;

    if (req.body.discount !== undefined) {
      if(parseInt(req.body.discount) < 0)
        return APIResponse.BadRequest(res,'Discount is less than 0')
      if(parseInt(req.body.discount) !== 0){
        if (!req.body.startedAt || !req.body.finishedAt)
          return APIResponse.BadRequest(
            res,
            "Must provide startedAt and finishedAt to discount"
          );
      }
      // we have 2 cases : 1 - add discount
      if (parseInt(req.body.discount) !== 0) {
        // search for already exist discount applied before for same course
        let existDiscount = await models.discount.findOne({
          subject: course.id,
          subjectType: 'course'
        });
        if (existDiscount) {
          console.log("before edit", existDiscount);
          await existDiscount
            .set({
              percentage: req.body.discount,
              startedAt: req.body.startedAt,
              finishedAt: req.body.finishedAt,
            })
            .save();
          console.log("after edit", existDiscount);
          delete req.body.discount;
        } else {
          // NO discount apply before on this course
          let newDiscount = await new models.discount({
            subject: course.id,
            subjectType: 'course',
            percentage: req.body.discount,
            startedAt: req.body.startedAt,
            finishedAt: req.body.finishedAt,
          }).save();
          console.log("newDiscount", newDiscount);
          req.body.discount = newDiscount.id;
        }
        // here assign new values of date to product
        req.body.discountStartedAt = req.body.startedAt
        req.body.discountFinishedAt = req.body.finishedAt
      } else {
        // remove discount (which mean discount = zero)
        delete req.body.discount;
        course.discount = undefined;
        course.hasOfferNow = false
        course.discountStartedAt = undefined
        course.discountFinishedAt = undefined
        await course.save();
        let prevDiscount = await models.discount.findOne({
          subject: course.id,
          subjectType: 'course'
        });
        if (prevDiscount) await prevDiscount.delete();
      }
      delete req.body.finishedAt;
      delete req.body.startedAt;
    }

    await course.set(req.body).save();
    let coursePopulated = await models.course.populate(course, "discount");
    return APIResponse.Ok(res, coursePopulated);
  }
);
