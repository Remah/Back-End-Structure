const $baseCtrl = require("../$baseCtrl");
const models = require("../../models");
const { APIResponse } = require("../../utils");

module.exports = $baseCtrl(async (req, res) => {
  const id = parseInt(req.params.id);
  if (isNaN(id)) return APIResponse.NotFound(res);

  const course = await models.course.findById(id);

  if (!course) return APIResponse.NotFound(res, "No course with that id");

  await course.delete();

  return APIResponse.NoContent(res);
});
