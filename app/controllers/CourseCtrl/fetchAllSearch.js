const $baseCtrl = require("../$baseCtrl");
const models = require("../../models");
const { APIResponse } = require("../../utils");
const { escapeRegExp } = require("lodash");

module.exports = $baseCtrl(async (req, res) => {
  let query = {}
  query.status = 'accepted'
  query.visibility = true
  if(req.query.name){
    delete req.queryFilter.name;
     query.$or = [{ nameAr: new RegExp(escapeRegExp(req.query.name), "i") },
             { nameEn:  new RegExp(escapeRegExp(req.query.name), "i") }]
  }

  if (req.query.rating) {
    delete req.queryFilter.rating;
    let rate = parseInt(req.query.rating);
    let queryRating = { $gte : rate }
    query.rating = queryRating;
    console.log(queryRating)
  }


  if(req.query.paid !== undefined)
  {
    delete req.queryFilter.paid
    query.price = req.query.paid === 'true' ?  { $gt : 0 } :  0 
  }

  if(req.query.language){
    delete req.queryFilter.language;
     query.language =  new RegExp(escapeRegExp(req.query.language), "i") 
  }
  delete req.queryFilter.deviceToken
  console.log(query)
  const courses = await models.course.fetchAll(
    req.allowPagination,
    {
      ...query,
      ...req.queryFilter // levels 
    },
    {
      ...req.queryOptions,
      populate: [{path: "teacher" , select: 'username photo'}, "category","subCategory",'secondSubCategory','discount']
    }
  )

  if(courses.docs[0]){
    if(!req.authenticatedUser){
      // two cases : 1. has prev doc , 2. generate new doc
      let document = await models.recommend.findOne({deviceToken: req.query.deviceToken})
      if(document){
        document.lastCategorySearch = courses.docs[0].secondSubCategory.id
        await document.save()
      } else {
          document = await new models.recommend({deviceToken: req.query.deviceToken,lastCategorySearch:courses.docs[0].secondSubCategory.id}).save()
      }
    } else {
        // Authoirzed user
        req.authenticatedUser.lastCategorySearch = courses.docs[0].secondSubCategory.id
        await req.authenticatedUser.save()
    }
  }
  return APIResponse.Ok(res,courses)

    
});
