const $baseCtrl = require("../$baseCtrl");
const models = require("../../models");
const { APIResponse } = require("../../utils");

module.exports = $baseCtrl(async (req, res) => {
  const course = await models.course.findById(parseInt(req.params.id))
  if(!course) return APIResponse.NotFound(res,'No course with that id')

  // filter expire discounts and remove them
//   let allDiscounts = await models.discount.find();
//   for (let i = 0; i < allDiscounts.length; i++) {
//     let course = await models.course.findById(allDiscounts[i].course);
//     if (!course) continue;
//     if (allDiscounts[i].isExpired) {
//       course.discount = undefined;
//       course.hasOfferNow = false;
//       await allDiscounts[i].delete();
//     } else if (allDiscounts[i].isValid) {
//       course.hasOfferNow = true;
//     } else {
//       // offer not start
//       course.hasOfferNow = false;
//     }
//     await course.save();
//   }

  const courses = await models.course.fetchAll(
    req.allowPagination,
    { category: course.category,  _id : {$ne : course.id} , visibility: true,status: 'accepted' },
    { ...req.queryOptions, populate: [{path: "teacher" , select: 'username photo'}, "category",'discount',"subCategory",'secondSubCategory'] }
  );

  return APIResponse.Ok(res, courses);
});
