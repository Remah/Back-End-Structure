const $baseCtrl = require("../$baseCtrl");
const models = require("../../models");
const { APIResponse } = require("../../utils");

module.exports = $baseCtrl(async (req, res) => {
  const teacherId = parseInt(req.params.id)
  if(isNaN(teacherId)) return APIResponse.NotFound(res)

  let teacher = await models.teacher.findById(teacherId)
  if(!teacher) return APIResponse.NotFound(res,'No teacher with that id')
  const courses = await models.course.find({ teacher: teacherId,status: 'accepted',visibility: true })
  .populate(["category","subCategory",'secondSubCategory',{path: "teacher" , select: 'username photo'},'discount'])
  teacher = teacher.toJSON()
  let canRate = false
  if(req.me){
      let progress =  await models._progress.findOne({user: req.me.id , teacher: teacherId})
      if(progress) canRate = true
      if(canRate){
        let prevRate = await models.rate.findOne({
          user: req.me.id,
          subjectType: 'teacher',
          subject: teacherId,
        });
        teacher.prevRate = prevRate ? prevRate : undefined
      }
  }
  let numberOfStudents = await models.courseProgress.countDocuments({teacher: teacherId})
  teacher.canRate = canRate
  teacher.numberOfCourses = courses.length
  teacher.numberOfStudents = numberOfStudents
  teacher.courses = courses
  return APIResponse.Ok(res, teacher);
});
