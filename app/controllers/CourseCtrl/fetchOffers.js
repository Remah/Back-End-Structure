const $baseCtrl = require("../$baseCtrl");
const models = require("../../models");
const { APIResponse } = require("../../utils");

module.exports = $baseCtrl(async (req, res) => {
  // // filter expire discounts and remove them
  // let allDiscounts = await models.discount.find({subjectType:'course'});
  // // filter expire discounts and remove them
  // for (let i = 0; i < allDiscounts.length; i++) {
  //   let course = await models.course.findById(allDiscounts[i].subject);
  //   if (!course) continue;
  //   if (allDiscounts[i].isExpired) {
  //     course.discount = undefined;
  //     course.hasOfferNow = false;
  //     await allDiscounts[i].delete();
  //   } else if (allDiscounts[i].isValid) {
  //     course.hasOfferNow = true;
  //   } else {
  //     // offer not start
  //     course.hasOfferNow = false;
  //   }
  //   await course.save();
  // }

  const courses = await models.course.fetchAll(
    req.allowPagination,
    { 
      status: 'accepted',
      hasOfferNow: true,
      visibility: true,
      discountStartedAt :{ $lte : new Date(new Date().toUTCString())},
      discountFinishedAt : { $gt : new Date(new Date().toUTCString())}
    },
    { ...req.queryOptions, populate: [{path: "teacher" , select: 'username photo'}, "category","subCategory",'secondSubCategory', "discount"] }
  );

  return APIResponse.Ok(res, courses);
});
