const $baseCtrl = require("../$baseCtrl");
const models = require("../../models");
const { APIResponse } = require("../../utils");

module.exports = $baseCtrl(async (req, res) => {
    let categoryId, category;
    let subCategoryId, subCategory;
    let secondSubCategoryId , secondSubCategory;
    let query = {}
    query.visibility = true;
    query.status = 'accepted'
   
    // fetch by ctegory
    if (req.query.category !== undefined) {
      categoryId = parseInt(req.query.category);
      if (isNaN(categoryId)) return APIResponse.NotFound(res);
      category = await models.category.findById(categoryId);
      if (!category) return APIResponse.NotFound(res);
  
      query.category = categoryId;
    }
    // fetch by subCategory
    if (req.query.subCategory !== undefined) {
      subCategoryId = parseInt(req.query.subCategory);
      if (isNaN(subCategoryId)) return APIResponse.NotFound(res);
      subCategory = await models.subCategory.findById(subCategoryId);
      if (!subCategory) return APIResponse.NotFound(res);
  
      query.subCategory = subCategoryId;
    }

    // fetch by secondSubCategory
    if (req.query.secondSubCategory !== undefined) {
        secondSubCategoryId = parseInt(req.query.secondSubCategory);
        if (isNaN(secondSubCategoryId)) return APIResponse.NotFound(res);
        secondSubCategory = await models.secondSubCategory.findById(secondSubCategoryId);
        if (!secondSubCategory) return APIResponse.NotFound(res);
    
        query.secondSubCategory = secondSubCategoryId;
      }


    const courses = await models.course.fetchAll(
    req.allowPagination,
    query,
    {
         ...req.queryOptions,
          populate: [{path: "teacher" , select: 'username photo'}, "category","subCategory",'secondSubCategory','discount']
    }
    );

    return APIResponse.Ok(res, courses);
});
