const $baseCtrl = require("../$baseCtrl");
const models = require("../../models");
const { APIResponse } = require("../../utils");

module.exports = $baseCtrl(async (req, res) => {
  
  const courses = await models.course.fetchAll(
    req.allowPagination,
    { teacher: req.me.id },
    { ...req.queryOptions, populate: ["category","subCategory",'secondSubCategory'] }
  );

  return APIResponse.Ok(res, courses);
});
