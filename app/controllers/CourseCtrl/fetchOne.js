const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');

module.exports = $baseCtrl(async (req, res) => {
  const id = parseInt(req.params.id);
  if (isNaN(id)) return APIResponse.NotFound(res);

  let course = await models.course.findById(id).populate(['teacher', 'category', 'discount',"subCategory",'secondSubCategory']);
  if (!course) return APIResponse.NotFound(res, 'No course with that id');

  let hideDiscount = 0;
  if (course.discount) {
    if (course.discount.isExpired) {
      course.discount = undefined;
      course.hasOfferNow = false;
      let prevDiscount = await models.discount.findOne({ subject: course.id, subjectType: 'course' });
      if (prevDiscount) await prevDiscount.delete();
    } else if (course.discount.isValid) {
      course.hasOfferNow = true;
    } else {
      // offer not start
      hideDiscount = 1;
      course.hasOfferNow = false;
    }
    await course.save();
  }
  if (hideDiscount) {
    course = course.toJSON({ hide: 'discount' });
  }
  return APIResponse.Ok(res, course);
});
