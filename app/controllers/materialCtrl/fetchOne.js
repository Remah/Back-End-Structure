const $baseCtrl = require("../$baseCtrl");
const models = require("../../models");
const { APIResponse } = require("../../utils");

module.exports = $baseCtrl(async (req, res) => {
  const material = await models.material.findById(req.params.id);
  if (!material) return APIResponse.NotFound(res, "No material with that id");

  // check if authorized
  if(req.me.role === 'student'){
    let progress = await models.liveProgress.findOne({user: req.me.id, live: material.live})
    if(!progress) return APIResponse.Forbidden(res,'You dont allow to view material of this live')
  }

  return APIResponse.Ok(res, material);
});
