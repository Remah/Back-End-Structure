const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');

module.exports = $baseCtrl(async (req, res) => {
  const material = await models.material
    .findById(req.params.id)
    .populate('live');
  if (!material) return APIResponse.NotFound(res);

  if (material.live.teacher !== req.me.id)
    return APIResponse.Forbidden(
      res,
      'Dont allow to remove this material',
    );

  await material.delete();

  return APIResponse.NoContent(res);
});
