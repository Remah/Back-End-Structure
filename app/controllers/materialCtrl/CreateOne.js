const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');
const digitaloceanspaces = require('../../services/digitaloceanspaces');

module.exports = $baseCtrl(
  [{ name: 'pdf', maxCount: 1 }],
  digitaloceanspaces,
  async (req, res) => {
    let routePath = req.route.path.split('/');
    let type =
      routePath[routePath.length - 1] === 'videos' ? 'video' : 'pdf';
    req.body.type = type;

    const live = await models.live.findById(req.params.id);
    if (!live) return APIResponse.NotFound(res, 'No Live with that id');

    if (live.teacher !== req.me.id)
      return APIResponse.Forbidden(
        res,
        'Dont allow to add material in this live course',
      );

    if (req.body.type === 'pdf') {
      if (req.files && req.files['pdf']) {
        req.body.link = req.files['pdf'][0].location;
      } else {
        return APIResponse.BadRequest(res, 'No pdf files uploaded');
      }
    } else if (req.body.type === 'video') {
      if (
        req.body.video === undefined ||
        req.body.duration === undefined
      ) {
        return APIResponse.BadRequest(
          res,
          'Video link and duration are required',
        );
      }
      req.body.link = req.body.video;
    }

    req.body.live = live.id;

    const material = await new models.material(req.body).save();
    return APIResponse.Created(res, material);
  },
);
