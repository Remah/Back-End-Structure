const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');
const digitaloceanspaces = require('../../services/digitaloceanspaces');

module.exports = $baseCtrl(
  [{ name: 'pdf', maxCount: 1 }],
  digitaloceanspaces,
  async (req, res) => {
    let routePath = req.route.path.split('/');
    let type = routePath[1] === 'videos' ? 'video' : 'pdf';

    req.body.type = type;
    delete req.body.live;

    const material = await models.material
      .findById(req.params.id)
      .populate('live');
    if (!material) return APIResponse.NotFound(res);

    if (material.live.teacher !== req.me.id)
      return APIResponse.Forbidden(
        res,
        'Dont allow to update this material',
      );

    if (req.body.type === 'pdf') {
      if (req.files && req.files['pdf']) {
        req.body.link = req.files['pdf'][0].location;
      }
    } else if (req.body.type === 'video') {
      if (req.body.video !== undefined) {
        req.body.link = req.body.video;
      }
    }

    await material.set(req.body).save();

    return APIResponse.Ok(res, material);
  },
);
