const $baseCtrl = require('../$baseCtrl');
const models = require('../../models');
const { APIResponse } = require('../../utils');

module.exports = $baseCtrl(async (req, res) => {
  let routePath = req.route.path.split('/');
  let type =
    routePath[routePath.length - 1] === 'videos' ? 'video' : 'pdf';

  const live = await models.live.findById(req.params.id);
  if (!live) return APIResponse.NotFound(res);

  // check if authorized
  if(req.me.role === 'student'){
    let progress = await models.liveProgress.findOne({user: req.me.id, live: live.id})
    if(!progress) return APIResponse.Forbidden(res,'You dont allow to view material of this live')
  }

  const materials = await models.material
    .find({ live: live.id, type })
    // .populate('live');

  return APIResponse.Ok(res, materials);
});
