const aws = require('aws-sdk');
const multer = require('multer');
const crypto = require('crypto');
const multerS3 = require('multer-s3');

module.exports = multerS3({
    s3: new aws.S3({
      accessKeyId: process.env.DO_SPACES_KEY,
      secretAccessKey: process.env.DO_SPACES_SECRET,
      endpoint: process.env.DO_SPACES_ENDPOINT,
      signatureVersion: 'v4',
    }),
    bucket: 'goud',
    contentType: multerS3.AUTO_CONTENT_TYPE,
    acl: 'public-read',
    key: (req, file, cb) => {
      const raw = crypto.pseudoRandomBytes(8)
      const fileName = Date.now().toString(); 
      cb(null, `${raw.toString('hex')}/${fileName}/${file.originalname}`);
    },
});
